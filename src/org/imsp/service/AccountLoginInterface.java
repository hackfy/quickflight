package org.imsp.service;

import org.imsp.businessdomain.account.Account;
import org.imsp.businessdomain.repository.AccountRepository;
import org.imsp.businessdomain.repository.xml.XmlAccountRepository;

public abstract class AccountLoginInterface{
	public Account login(String type, String mail, String passwd){
		AccountRepository repository = new XmlAccountRepository();
		Account account = repository.getAccount(mail, passwd);
		if(account != null){
			if(account.getAccountState().equals("disactived"))
				account =  null;
			else if(!account.getType().equalsIgnoreCase(type))
			account = null;
		}
		return account;
	}
	
	public void logout(Account account){
		account.updateAccount();
		account.destructAccount();
	}
	
	public abstract Account createAccount(String id, String firstname, String lastname, String sex, String currenttown, String mail, String passwd);
	
	public boolean removeAccount(String type, String mail, String passwd){
		Account account = this.login(type, mail, passwd);
		if(account == null)
			return false;
		if(!account.removeAccount())
			return false;
		return true;
	}
	
	public boolean updateAccountOwnerPasswd(String type, String mail, String oldpasswd, String newpasswd){
		Account account = this.login(type, mail, oldpasswd);
		if(account == null)
			return false;
		account.setUserPasswd(newpasswd);
		if(!account.updateAccount())
			return false;
		return true;
	
	}
	
	public boolean updateAccountOwnerMail(String type, String mail, String passwd, String newmail){
		Account account = this.login(type, mail, passwd);
		if(account == null)
			return false;
		account.setUserMail(newmail);
		if(!account.updateAccount())
			return false;
		return true;
	
	}
}