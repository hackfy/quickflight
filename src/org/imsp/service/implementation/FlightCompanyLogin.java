package org.imsp.service.implementation;

import org.imsp.businessdomain.flight.FlightCompany;
import org.imsp.businessdomain.repository.FlightCompanyRepository;
import org.imsp.businessdomain.repository.xml.XmlFlightCompanyRepository;
import org.imsp.service.FlightCompanyLoginInterface;

public class FlightCompanyLogin implements FlightCompanyLoginInterface{

	public FlightCompany login(String mail, String passwd) {
		FlightCompanyRepository repository = new XmlFlightCompanyRepository();
		FlightCompany company = repository.getFlightCompany(mail, passwd);
		return company;
	}

	public FlightCompany createFlightCompany(String id, String companyname, String mail, String passwd) {
		FlightCompany company = new FlightCompany(id, companyname, mail, passwd);
		if(!company.saveFlightCompany())
			return null;
		return company;
	}

	public boolean removeFlightCompany(String mail, String passwd) {
		FlightCompany company = this.login(mail, passwd);
		if(company == null)
			return false;
		if(!company.removeFlightCompany())
			return false;
		return true;
	}

	public boolean updateFlightCompanyPasswd(String mail, String oldpasswd, String newpasswd) {
		FlightCompany company = this.login(mail, oldpasswd);
		if(company == null)
			return false;
		company.setCompanyPasswd(newpasswd);
		if(!company.updateFlightCompany())
			return false;
		return true;
	}

	public boolean updateFlightCompanyMail(String mail, String passwd, String newmail) {
		FlightCompany company = this.login(mail, passwd);
		if(company == null)
			return false;
		company.setCompanyMail(newmail);
		if(!company.updateFlightCompany())
			return false;
		return true;
	}

	public FlightCompany getFlightCompanyByID(String id) {
		FlightCompanyRepository repository = new XmlFlightCompanyRepository();
		FlightCompany company = repository.getFlightCompany(id);
		return company;
	}

}