package org.imsp.service.implementation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.SeatClass;
import org.imsp.businessdomain.repository.FlightRepository;
import org.imsp.businessdomain.repository.xml.XmlFlightRepository;
import org.imsp.service.FlightServicesInterface;


public class FlightServices implements FlightServicesInterface {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");

	private boolean hasBeenSortedByNumberOfReservedSeats(List<Flight> flights, String category){
		for(int i=0; i < flights.size()-1; i++) {
			Flight currentFlight = flights.get(i);
			Flight nextFlight = flights.get(i+1);
			if(category == null){
				if((currentFlight.getNumberOfSeats() - currentFlight.getNumberOfReservedSeats()) > (nextFlight.getNumberOfSeats() - nextFlight.getNumberOfReservedSeats())){
					return false;
				}
			}
			else{
				if((currentFlight.getNumberOfSeatsByClass(category) - currentFlight.getNumberOfReservedSeatsByClass(category)) > (nextFlight.getNumberOfSeatsByClass(category) - nextFlight.getNumberOfReservedSeatsByClass(category))){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean hasBeenSortedBySeatPrice(List<Flight> flights, String category){
		if(category == null)
			category = "Regular";
		for(int i=0; i < flights.size()-1; i++) {
			Flight currentFlight = flights.get(i);
			Flight nextFlight = flights.get(i+1);
			if(currentFlight.getSeatPrice(category) > nextFlight.getSeatPrice(category)){
				return false;
			}
		}
		return true;
	}
	
	private List<Flight> sortFlightsByNumberOfReservedSeats(List<Flight> flights, String category){
		int count = 0;
		int lenght = 0;
		while(!this.hasBeenSortedByNumberOfReservedSeats(flights, category)){
			lenght = flights.size() - count;
			for(int i=0; i < lenght-1; i++) {
				Flight currentFlight = flights.get(i);
				Flight nextFlight = flights.get(i+1);
				if(category == null){
					if((currentFlight.getNumberOfSeats() - currentFlight.getNumberOfReservedSeats()) > (nextFlight.getNumberOfSeats() - nextFlight.getNumberOfReservedSeats())){
						flights.set(i, nextFlight);
						flights.set(i+1, currentFlight);
					}
				}
				else{
					if((currentFlight.getNumberOfSeatsByClass(category) - currentFlight.getNumberOfReservedSeatsByClass(category)) > (nextFlight.getNumberOfSeatsByClass(category) - nextFlight.getNumberOfReservedSeatsByClass(category))){
						flights.set(i, nextFlight);
						flights.set(i+1, currentFlight);
					}
				}
			}
			count++;
		}
		return flights;
	}
	private List<Flight> sortFlightsBySeatPrice(List<Flight> flights, String category){
		int count = 0;
		int lenght = 0;
		if(category == null)
			category = "Regular";
		while(!this.hasBeenSortedBySeatPrice(flights, category)){
			lenght = flights.size() - count;
			for(int i=0; i < lenght-1; i++) {
				Flight currentFlight = flights.get(i);
				Flight nextFlight = flights.get(i+1);
				if(currentFlight.getSeatPrice(category) > nextFlight.getSeatPrice(category)){
					flights.set(i, nextFlight);
					flights.set(i+1, currentFlight);
				}
			}
			count++;
		}
		return flights;
	}
	
	public boolean canReachDate(String referenceDate, String date){
		try {
			long dateDiff = DATE_FORMAT.parse(date).getTime() - DATE_FORMAT.parse(referenceDate).getTime();
			if((dateDiff > 0 && dateDiff < 24*3600000))
				return true;
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private List<Flight> getFlightsWithLessTanOneDayDepartureDay(List<Flight> flights, String queryDate){
		List<Flight> list = new ArrayList<Flight>();
		if(queryDate == null)
			queryDate = DATE_FORMAT.format(new Date());
		for(int i=0; i < flights.size(); i++){
			String flightDate = flights.get(i).getFlightDepartureDate();
			if(this.canReachDate(queryDate, flightDate)){
				list.add(flights.get(i));
			}
		}
		return list;
	}
	private List<Flight> getFlightsWithMoreThanOneDayDifferenceDepartureDay(List<Flight> flights, String queryDate){
		List<Flight> list = new ArrayList<Flight>();
		if(queryDate == null)
			queryDate = DATE_FORMAT.format(new Date());
		for(int i=0; i < flights.size(); i++){
			String flightDate = flights.get(i).getFlightDepartureDate();
			if(!this.canReachDate(queryDate, flightDate)){
				list.add(flights.get(i));
			}
		}
		return list;
	}
	
	public List<Flight> searchFlights(String departure, String arrival, String queryDate, String category){
		FlightRepository repository = new XmlFlightRepository();
		List<Flight> list = repository.getSpecificFlights(departure, arrival, queryDate);
		boolean found = false;
		if(category !=  null){
			for(int i=0; i < list.size(); i++) {
				Flight flight = list.get(i);
				if(flight.getName().equalsIgnoreCase("AirMoyen") || flight.getName().equalsIgnoreCase("AirLourd")){
					List<SeatClass> seats = flight.getFlightSeatsClass();
					for(SeatClass seat:seats){
						if(seat.getSeatClassName().equalsIgnoreCase(category) && (seat.getNumberOfReservedSeats() < seat.getNumberOfSeats())) {
							found = true;
							break;
						}
					}
				}
				if(!found) {
					list.remove(i);
					i--;
				}
			}
		}
		else{
			for(int i=0; i < list.size(); i++) {
				Flight flight = list.get(i);
				if(flight.getNumberOfReservedSeats() >= flight.getNumberOfSeats()) {
					list.remove(i);
					i--;
				}
			}
		}
		List<Flight> list1 = this.getFlightsWithLessTanOneDayDepartureDay(list, queryDate);
		list1 = this.sortFlightsByNumberOfReservedSeats(list1, category);
		List<Flight> list2 = this.getFlightsWithMoreThanOneDayDifferenceDepartureDay(list, queryDate);
		list2 = this.sortFlightsBySeatPrice(list2, category);
		list1.addAll(list2);
		return list1;
	}
	
	public List<Flight> searchReturnFlights(String departure, String arrival, String queryDate, String category){
		return this.searchFlights(arrival, departure, queryDate, category);
	}
	
	public List<Flight> searchFlightsWithWeight(String departure, String arrival, String queryDate, String category, double weight){
		List<Flight> list = new ArrayList<Flight>();
		list = this.searchFlights(departure, arrival, queryDate, category);
		if(weight > 23.5 && weight < 42.5){
			for(int i=0; i < list.size(); i++) {
		    	Flight flight = list.get(i);
		    	if (!flight.getName().equalsIgnoreCase("AirMoyen") && !flight.getName().equalsIgnoreCase("AirLourd")) {
		    			list.remove(i);
		    			i--;
		    	}
			}
		}
		else if(weight >= 42.5 && weight <= 65){
			for(int i=0; i < list.size(); i++) {
		    	Flight flight = list.get(i);
		    	if (!flight.getName().equalsIgnoreCase("AirLourd")) {
		    			list.remove(i);
		    			i--;
		    	}
			}
		}
		else if(weight > 65){
			for(int i=0; i < list.size(); i++) {
		    	Flight flight = list.get(i);
		    	if (!flight.getName().equalsIgnoreCase("AirLourd") || flight.getFlightExceedingWeight() < (weight - flight.getFlightMaxWeight())) {
		    			list.remove(i);
		    			i--;
		    	}
			}
		}
		return list;
	}
	
	public double reserveFlight(String flightid, String seatClass, int count) {
		FlightRepository repository = new XmlFlightRepository();
		Flight flight = repository.getFlight(flightid);
		if(!flight.reserveSeats(seatClass, count))
			return -1.0;
		return count*flight.getSeatPrice(seatClass);
	}
}
