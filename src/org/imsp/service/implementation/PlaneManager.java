package org.imsp.service.implementation;

import java.util.List;

import org.imsp.businessdomain.flight.Plane;
import org.imsp.businessdomain.repository.PlaneRepository;
import org.imsp.businessdomain.repository.xml.XmlPlaneRepository;
import org.imsp.service.PlaneManagerInterface;


public class PlaneManager implements PlaneManagerInterface {

	public Plane addPlane(String planeid, String planemodel, String company, String[] seatsclass, int[] seatsnumber) {
		Plane plane = new Plane(planeid, planemodel, company, seatsclass, seatsnumber);
		if(!plane.savePlane())
			return null;
		return plane;
	}

	public boolean updatePlane(Plane plane) {
		return plane.updatePlane();
	}

	public boolean removePlane(Plane plane) {
		return plane.removePlane();
	}

	public Plane getPlane(String planeid) {
		PlaneRepository repository = new XmlPlaneRepository();
		return repository.getPlane(planeid);
	}

	public List<Plane> getAllPlanes() {
		PlaneRepository repository = new XmlPlaneRepository();
		return repository.getAllPlanes();
	}
	
	public List<Plane> getAllPlanes(String company) {
		List<Plane> planes = this.getAllPlanes();
		if(company != null){
			Plane plane = null;
			for(int i = 0; i < planes.size(); i++){
				plane = planes.get(i);
				if(!plane.getPlaneCompany().equals(company)){
					planes.remove(i);
					i--;
				}
			}
		}
		return planes;
	}

	public int setPlaneNumberOfSeatsBySeatClass(Plane plane, String seatclass, int number) {
		plane.setNumberOfSeatsByClass(seatclass, number);
		plane.updatePlane();
		if(plane.getNumberOfSeatsByClass(seatclass) != number)
			return -1;
		return plane.getNumberOfSeatsByClass(seatclass);
	}

}
