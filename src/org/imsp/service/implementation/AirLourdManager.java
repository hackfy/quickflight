package org.imsp.service.implementation;

import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.repository.FlightRepository;
import org.imsp.businessdomain.repository.xml.XmlFlightRepository;
import org.imsp.service.AirLourdManagerInterface;

public class AirLourdManager implements AirLourdManagerInterface{
	public List<Flight> getAllAirLourd() {
		FlightRepository repository = new XmlFlightRepository();
		List<Flight> flights = repository.getAllFlights();
		for(int i=0; i < flights.size(); i++) {
	    	Flight flight = flights.get(i);
	    	if (!flight.getName().equalsIgnoreCase("AirLourd")) {
	    			flights.remove(i);
	    			i--;
	    	}
		}
		return flights;
	}

	public Flight getSpecificAirLourd(String airlourdid) {
		List<Flight> airlourds = this.getAllAirLourd();
		for(Flight airlourd:airlourds){
			if(airlourd.getFlightID().equalsIgnoreCase(airlourdid)){
				return airlourd;
			}
		}
		return null;
	}

	public double setAirLourdExceedingWeight(Flight airlourd, double weight) {
		airlourd.setFlightExceedingWeight(weight);
		airlourd.updateFlight();
		return airlourd.getFlightExceedingWeight();
	}
	
	public static void main(String[] args) {
		AirLourdManager manager = new AirLourdManager();
		List<Flight> airlourd = manager.getAllAirLourd();
		System.out.println(airlourd);
	}

}
