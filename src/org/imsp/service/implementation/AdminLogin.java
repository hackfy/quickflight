package org.imsp.service.implementation;

import org.imsp.businessdomain.account.Account;
import org.imsp.businessdomain.account.AccountFactory;
import org.imsp.service.AccountLoginInterface;

public class AdminLogin extends AccountLoginInterface{
	public Account login(String mail, String passwd){
		return super.login("admin", mail, passwd);
	}
	
	public Account createAccount(String id, String firstname, String lastname, String sex, String currenttown, String mail, String passwd) {
		AccountFactory factory = new AccountFactory();
		Account account = factory.createAccount("admin", id, firstname, lastname, sex, currenttown, mail, passwd);
		if(!account.saveAccount())
			return null;
		return account;
	}
	
	public boolean removeAccount(String mail, String passwd){
		return super.removeAccount("admin", mail, passwd);
	}
	
	public boolean updateAccountOwnerPasswd(String mail, String oldpasswd, String newpasswd){
		return super.updateAccountOwnerPasswd("admin", mail, oldpasswd, newpasswd);
	}
	
	public boolean updateAccountOwnerMail(String type, String mail, String passwd, String newmail){
		return super.updateAccountOwnerMail("admin", mail, passwd, newmail);
	}
}
