package org.imsp.service.implementation;

import java.util.Date;
import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.FlightFactory;
import org.imsp.businessdomain.repository.FlightRepository;
import org.imsp.businessdomain.repository.xml.XmlFlightRepository;
import org.imsp.service.FlightManagerInterface;

public class FlightManager implements FlightManagerInterface{
	
	public Flight addFlight(String type, String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date flightdate){
		FlightFactory factory = new FlightFactory();
		Flight flight = factory.createFlight(type, flight_id, company, plane, max_weight, departure_town, arrival_town, flightdate);
		if(!flight.saveFlight())
			return null;
		return flight;
	}

	public boolean updateFlight(Flight flight) {
		return flight.updateFlight();
	}

	public boolean removeFlight(Flight flight) {
		return flight.removeFlight();
	}

	public Flight getFlight(String flightid) {
		FlightRepository repository = new XmlFlightRepository();
		return repository.getFlight(flightid);
	}
	
	public List<Flight> getAllFlights() {
		FlightRepository repository = new XmlFlightRepository();
		return repository.getAllFlights();
	}

	public boolean updateFlightStatus(Flight flight, String status) {
		flight.setFlightStatus(status);
		return flight.updateFlight();
	}

	public boolean setFlightSeatsPrice(Flight flight, String seatClass,double price) {
		flight.setSeatPrice(seatClass, price);
		return flight.updateFlight();
	}
	
	public static void main(String[] args) {
		FlightManager manager = new FlightManager();
		Flight flight = manager.getFlight("AF4");
		
		manager.setFlightSeatsPrice(flight, "Economic", 75);
		manager.setFlightSeatsPrice(flight, "Regular", 100);
		manager.setFlightSeatsPrice(flight, "Business", 250);
		
		manager.updateFlightStatus(flight, "Waiting");
		System.out.println(flight.getSeatPrice("Business"));
	}

}
