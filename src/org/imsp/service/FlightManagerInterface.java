package org.imsp.service;

import java.util.Date;
import java.util.List;

import org.imsp.businessdomain.flight.Flight;

public interface FlightManagerInterface {
	public Flight addFlight(String type, String flight_id, String company, String plane, double max_weight, String departure, String arrival, Date flightate);
	public boolean updateFlight(Flight flight);
	public boolean updateFlightStatus(Flight flight, String status);
	public boolean removeFlight(Flight flight);
	public Flight getFlight(String flightid);
	public List<Flight> getAllFlights();
	public boolean setFlightSeatsPrice(Flight flight, String seatClass, double price);
}
