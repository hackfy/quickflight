package org.imsp.service;

import java.util.List;

import org.imsp.businessdomain.flight.Plane;

public interface PlaneManagerInterface {
	public Plane addPlane(String planeid, String planemodel, String company, String[] seatsclass, int[] seatsnumber);
	public boolean updatePlane(Plane plane);
	public boolean removePlane(Plane plane);
	public Plane getPlane(String planeid);
	public List<Plane> getAllPlanes();
	public List<Plane> getAllPlanes(String company);
	public int setPlaneNumberOfSeatsBySeatClass(Plane plane, String seatclass, int number);
}
