package org.imsp.service;

import java.util.List;

import org.imsp.businessdomain.flight.Flight;

public interface FlightServicesInterface{
	public List<Flight> searchFlights(String departure, String arrival, String queryDate, String category);
	public List<Flight> searchReturnFlights(String departure, String arrival, String queryDate, String category);
	public List<Flight> searchFlightsWithWeight(String departure, String arrival, String queryDate, String category, double weight);
	public double reserveFlight(String flightid, String seatClass, int count);
}
