package org.imsp.service;

import java.util.List;

import org.imsp.businessdomain.flight.Flight;

public interface AirLourdManagerInterface {
	public List<Flight> getAllAirLourd();
	public Flight getSpecificAirLourd(String flightid);
	public double setAirLourdExceedingWeight(Flight airlourd, double weight);
}
