package org.imsp.flight;

import java.util.List;

import org.imsp.dao.XmlPlaneDAO;
import org.imsp.repository.PlaneRepository;
import org.imsp.repository.XmlPlaneRepository;


public class PlaneManager implements PlaneManagerInterface {

	public Plane addPlane(String planeid, String planemodel, String company, String[] seatsclass, int[] seatsnumber) {
		Plane plane = new Plane(planeid, planemodel, company, seatsclass, seatsnumber);
		if(!plane.savePlane())
			return null;
		return plane;
	}

	public boolean updatePlane(Plane plane) {
		return plane.updatePlane();
	}

	public boolean removePlane(Plane plane) {
		return plane.removePlane();
	}

	public Plane getPlane(String planeid) {
		PlaneRepository repository = new XmlPlaneRepository(new XmlPlaneDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/planes.xml"));
		return repository.getPlane(planeid);
	}

	public List<Plane> getAllPlanes() {
		PlaneRepository repository = new XmlPlaneRepository(new XmlPlaneDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/planes.xml"));
		return repository.getAllPlanes();
	}
	
	public List<Plane> getAllPlanes(String company) {
		List<Plane> planes = this.getAllPlanes();
		if(company != null){
			Plane plane = null;
			for(int i = 0; i < planes.size(); i++){
				plane = planes.get(i);
				if(!plane.getPlaneCompany().equals(company)){
					planes.remove(i);
					i--;
				}
			}
		}
		return planes;
	}

	public int setPlaneNumberOfSeatsBySeatClass(Plane plane, String seatclass, int number) {
		plane.setNumberOfSeatsByClass(seatclass, number);
		plane.updatePlane();
		if(plane.getNumberOfSeatsByClass(seatclass) != number)
			return -1;
		return plane.getNumberOfSeatsByClass(seatclass);
	}

}
