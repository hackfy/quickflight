package org.imsp.flight;

import org.imsp.dao.XmlFlightCompanyDAO;
import org.imsp.repository.FlightCompanyRepository;
import org.imsp.repository.XmlFlightCompanyRepository;

public class FlightCompany {
	private String companyID;
	private String companyName;
	private String companyMail;
	private String companyPasswd;
	
	public FlightCompany(String company_id, String company_name, String company_mail, String company_passwd){
		this.companyID = company_id;
		this.companyName = company_name;
		this.companyMail = company_mail;
		this.companyPasswd = company_passwd;
	}
	
	public String getCompanyID(){
		return this.companyID;
	}
	public void setCompanyID(String value){
		this.companyID = value;
	}
	
	public String getCompanyName(){
		return this.companyName;
	}
	public void setCompanyName(String value){
		this.companyName = value;
	}
	
	public String getCompanyMail(){
		return this.companyMail;
	}
	public void setCompanyMail(String value){
		this.companyMail = value;
	}
	
	public String getCompanyPasswd(){
		return this.companyPasswd;
	}
	public void setCompanyPasswd(String value){
		this.companyPasswd = value;
	}
	
	public boolean saveFlightCompany(){
		FlightCompanyRepository repository = new XmlFlightCompanyRepository(new XmlFlightCompanyDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/companies.xml"));
		return repository.addFlightCompany(this);
	}
	
	public boolean updateFlightCompany(){
		FlightCompanyRepository repository = new XmlFlightCompanyRepository(new XmlFlightCompanyDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/companies.xml"));
		return repository.updateFlightCompany(this);
	}
	
	public boolean removeFlightCompany(){
		FlightCompanyRepository repository = new XmlFlightCompanyRepository(new XmlFlightCompanyDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/companies.xml"));
		return repository.removeFlightCompany(this);
	}
}
