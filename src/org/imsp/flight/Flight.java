package org.imsp.flight;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.imsp.dao.XmlFlightDAO;
import org.imsp.dao.XmlPlaneDAO;
import org.imsp.repository.FlightRepository;
import org.imsp.repository.PlaneRepository;
import org.imsp.repository.XmlFlightRepository;
import org.imsp.repository.XmlPlaneRepository;

public abstract class Flight {
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
	protected String flightID;
	protected String company;
	protected String plane;
	protected double maxWeight;
	protected String departureTown;
	protected String arrivalTown;
	protected String departureDate;
	protected String status;
	protected List<SeatClass> seatsClass;
	
	public Flight(){
		this.flightID = null;
		this.company = null;
		this.plane = null;
		this.maxWeight = 0.0;
		this.departureTown = null;
		this.arrivalTown = null;
		this.departureDate = null;
		this.status = null;
		this.seatsClass = null;
	}
	public Flight(String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date departure_date){
		this.flightID = flight_id;
		this.company = company;
		this.plane = plane;
		this.maxWeight = max_weight;
		this.departureTown = departure_town;
		this.arrivalTown = arrival_town;
		this.departureDate = DATE_FORMAT.format(departure_date);
		this.status = "Waiting";
		PlaneRepository repository = new XmlPlaneRepository(new XmlPlaneDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/planes.xml"));
		Plane planeObject = repository.getPlane(plane);
		this.seatsClass = planeObject.getPlaneSeats();
	}
	
	public abstract String getName();
	
	public String getFlightID(){
		return this.flightID;
	}
	public void setFlightID(String value){
		this.flightID = value;
	}
	public String getFlightCompany(){
		return this.company;
	}
	public void setFlightCompany(String value){
		this.company = value;
	}
	public String getFlightPlane(){
		return this.plane;
	}
	public void setFlightPlane(String plane){
		this.plane = plane;
	}
	
	public double getFlightMaxWeight(){
		return this.maxWeight;
	}
	public void setFlightMaxWeight(double value){
		this.maxWeight = value;
	}
	public abstract double getFlightExceedingWeight();
	public abstract void setFlightExceedingWeight(double weight);
	
	public String getFlightDepartureTown(){
		return this.departureTown;
	}
	public void setFlightDepartureTown(String value){
		this.departureTown = value;
	}
	
	public String getFlightArrivalTown(){
		return this.arrivalTown;
	}
	public void setFlightArrivalTown(String value){
		this.arrivalTown = value;
	}
	
	public String getFlightDepartureDate(){
		return this.departureDate;
	}
	public void setFlightDepartureDate(String value){
		this.departureDate = value;
	}
	
	public String getFlightStatus(){
		return this.status;
	}
	public void setFlightStatus(String value){
		this.status = value;
	}
	
	public List<SeatClass> getFlightSeatsClass(){
		return this.seatsClass;
	}
	
	public double getSeatPrice(String category){
		double price = 0;
		for(int i=0; i< this.seatsClass.size(); i++){
			if(this.seatsClass.get(i).getSeatClassName().equalsIgnoreCase(category)){
				price = this.seatsClass.get(i).getSeatPrice();
				break;
			}
		}
		return price;
	}
	public void setSeatPrice(String category, double price){
		for(int i=0; i< this.seatsClass.size(); i++){
			if(this.seatsClass.get(i).getSeatClassName().compareToIgnoreCase(category) == 0){
				this.seatsClass.get(i).setSeatPrice(price);
				break;
			}
		}
	}
	public int getNumberOfSeatsByClass(String category){
		List<SeatClass> list = this.seatsClass;
		for(SeatClass seat:list){
			if(seat.getSeatClassName().equalsIgnoreCase(category))
				return seat.getNumberOfSeats();
		}
		return 0;
	}
	public int getNumberOfSeats(){
		List<SeatClass> list = this.seatsClass;
		int count = 0;
		for(SeatClass seat:list){
			count += this.getNumberOfSeatsByClass(seat.getSeatClassName());
		}
		return count;
	}
	
	public int getNumberOfReservedSeatsByClass(String category){
		List<SeatClass> list = this.seatsClass;
		for(SeatClass seat:list){
			if(seat.getSeatClassName().equalsIgnoreCase(category))
				return seat.getNumberOfReservedSeats();
		}
		return 0;
	}
	
	public boolean reserveSeats(String category, int number){
		List<SeatClass> list = this.seatsClass;
		boolean bool = false;
		for(SeatClass seat:list){
			if(seat.getSeatClassName().equals(category)){
				if(seat.getNumberOfSeats() - seat.getNumberOfReservedSeats() > number){
					seat.setNumberOfReservedSeats(number);
					return true;
				}
				else
					bool = false;
				break;
			}
		}
		return bool;
	}
	public int getNumberOfReservedSeats(){
		List<SeatClass> list = this.seatsClass;
		int count = 0;
		for(SeatClass seat:list){
			count += this.getNumberOfReservedSeatsByClass(seat.getSeatClassName());
		}
		return count;
	}
	
	public boolean saveFlight(){
		FlightRepository repository = new XmlFlightRepository(new XmlFlightDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/flight.xml"));
		return repository.addFlight(this);
	}
	
	public boolean updateFlight(){
		FlightRepository repository = new XmlFlightRepository(new XmlFlightDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/flight.xml"));
		return repository.updateFlight(this);
	}
	
	public boolean removeFlight(){
		FlightRepository repository = new XmlFlightRepository(new XmlFlightDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/flight.xml"));
		return repository.removeFlight(this);
	}
}

class AirLourd extends Flight{
	private double exceedingWeight;
	public AirLourd(String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date departure_date){
		super(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
		this.exceedingWeight = 0.0;
	}
	
	public double getFlightExceedingWeight(){
		return this.exceedingWeight;
	}
	public void setFlightExceedingWeight(double weight){
		this.exceedingWeight = weight;
	}
	
	public String getName(){
		return new String("AirLourd");
	}
}
class AirMoyen extends Flight{
	public AirMoyen(String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date departure_date){
		super(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
	}
	
	public double getFlightExceedingWeight(){
		return 0.0;
	}
	public  void setFlightExceedingWeight(double weight){
		
	}
	public String getName(){
		return new String("AirMoyen");
	}
}

class AirLeger extends Flight{
	public AirLeger(String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date departure_date){
		super(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
	}
	
	public double getFlightExceedingWeight(){
		return 0.0;
	}
	public  void setFlightExceedingWeight(double weight){
		
	}
	public String getName(){
		return new String("AirLeger");
	}
}

class NullFlight extends Flight{
	public NullFlight(){
		super();
	}
	
	public double getFlightExceedingWeight(){
		return 0.0;
	}
	public void setFlightExceedingWeight(double weight){
	}
	public String getName(){
		return new String("NullFlight");
	}
}
