package org.imsp.flight;

import java.util.List;

import org.imsp.dao.XmlFlightDAO;
import org.imsp.repository.FlightRepository;
import org.imsp.repository.XmlFlightRepository;

public class AirLourdManager implements AirLourdManagerInterface{
	public List<Flight> getAllAirLourd() {
		FlightRepository repository = new XmlFlightRepository(new XmlFlightDAO("eclipse-workspace>/QuickFlight/src/org/imsp/persistance/flight.xml"));
		List<Flight> flights = repository.getAllFlights();
		for(int i=0; i < flights.size(); i++) {
	    	Flight flight = flights.get(i);
	    	if (!flight.getName().equalsIgnoreCase("AirLourd")) {
	    			flights.remove(i);
	    			i--;
	    	}
		}
		return flights;
	}

	public Flight getSpecificAirLourd(String airlourdid) {
		List<Flight> airlourds = this.getAllAirLourd();
		for(Flight airlourd:airlourds){
			if(airlourd.getFlightID().equalsIgnoreCase(airlourdid)){
				return airlourd;
			}
		}
		return null;
	}

	public double setAirLourdExceedingWeight(Flight airlourd, double weight) {
		airlourd.setFlightExceedingWeight(weight);
		airlourd.updateFlight();
		return airlourd.getFlightExceedingWeight();
	}
	
	public static void main(String[] args) {
		AirLourdManager manager = new AirLourdManager();
		Flight airlourd = manager.getAllAirLourd().get(1);
		manager.setAirLourdExceedingWeight(airlourd, 25);
		System.out.println(airlourd.getFlightExceedingWeight());
	}

}
