package org.imsp.flight;

public interface FlightCompanyLoginInterface {
	public FlightCompany login(String mail, String passwd);
	public FlightCompany createFlightCompany(String id, String companyname, String mail, String passwd);
	public boolean removeFlightCompany(String mail, String passwd);
	public boolean updateFlightCompanyPasswd(String mail, String oldpasswd, String newpasswd);
	public boolean updateFlightCompanyMail(String mail, String passwd, String newmail);
}
