package org.imsp.flight;

import java.util.Date;

public class FlightFactory {
	public Flight createFlight(String type, String flight_id, String company, String plane, double max_weight, String departure_town, String arrival_town, Date departure_date){
		Flight flight = new NullFlight();
		if(type.compareToIgnoreCase("AirLourd") == 0){
			max_weight = max_weight < 65.0 ? max_weight:65.0;
			flight = new AirLourd(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
		}
		else if(type.compareToIgnoreCase("AirMoyen") == 0){
			max_weight = max_weight < 42.0 ? max_weight:42.0;
			flight = new AirMoyen(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
		}
		else if(type.compareToIgnoreCase("AirLeger") == 0){
			max_weight = max_weight < 23.0 ? max_weight:23.0;
			flight = new AirLeger(flight_id, company, plane, max_weight, departure_town, arrival_town, departure_date);
		}
		return flight;
	}

}