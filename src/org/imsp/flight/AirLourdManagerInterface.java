package org.imsp.flight;

import java.util.List;

public interface AirLourdManagerInterface {
	public List<Flight> getAllAirLourd();
	public Flight getSpecificAirLourd(String flightid);
	public double setAirLourdExceedingWeight(Flight airlourd, double weight);
}
