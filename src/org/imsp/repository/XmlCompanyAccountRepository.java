package org.imsp.repository;

import org.imsp.dao.XmlCompanyAccountDAO;
import org.imsp.flight.FlightCompany;

import org.jdom.Element;

public class XmlCompanyAccountRepository implements CompanyAccountRepository{
	
	private XmlCompanyAccountDAO xmlCompanyAccountDAO;

	public XmlCompanyAccountRepository(XmlCompanyAccountDAO xmlCompanyAccountDAO) {
	    this.xmlCompanyAccountDAO = xmlCompanyAccountDAO;
	}

	public FlightCompany getAccount(String mail, String passwd){
		Element accountNode = this.xmlCompanyAccountDAO.getElement(mail, passwd);
		FlightCompany account = null;
		try {
			account = this.createAccountFromAccountNode(accountNode);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	    return account;
	}
	
	private FlightCompany createAccountFromAccountNode(final Element accountNode) throws Exception {
		FlightCompany account = new FlightCompany(accountNode.getChildText("id"),accountNode.getChildText("name"),accountNode.getChildText("mail"),accountNode.getChildText("password"));
		return account;
	}
}