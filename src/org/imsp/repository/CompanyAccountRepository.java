package org.imsp.repository;


import org.imsp.flight.FlightCompany;

public interface CompanyAccountRepository {
	public FlightCompany getAccount(String mail, String passwd);
	//public boolean updateAccount(String flight_id, Flight flight);
	//public boolean removeAccount(String flight_id);
}
