package org.imsp.repository;

import org.imsp.flight.*;

import java.util.List;


public interface FlightRepository {
	public List<Flight> getAllFlights();
	public List<Flight> getSpecificFlights(String departure, String arrival, String queryDate);
	public Flight getFlight(String flight_id);
	public boolean addFlight(Flight flight);
	public boolean updateFlight(Flight flight);
	public boolean removeFlight(Flight flight);
}