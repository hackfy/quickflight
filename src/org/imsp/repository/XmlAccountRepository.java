package org.imsp.repository;

import org.imsp.account.Account;
import org.imsp.account.AccountFactory;
import org.imsp.dao.XmlAccountDAO;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

public class XmlAccountRepository implements AccountRepository{
	private XmlAccountDAO xmlAccountDAO;

	public XmlAccountRepository(XmlAccountDAO xmlAccountDAO) {
	    this.xmlAccountDAO = xmlAccountDAO;
	}
	
	public List<Account> getAllAccounts(){
		List<Account> accounts = new ArrayList<Account>();
		List<Element> list = this.xmlAccountDAO.getAllAccountElements();
		for(Element accountNode:list){
			try {
				accounts.add(this.createAccountFromAccountNode(accountNode));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return accounts;
	}
	
	public Account getAccount(String mail, String passwd){
		Element accountNode = this.xmlAccountDAO.getAccountElement(mail, passwd);
		Account account = null;
		try {
			account = this.createAccountFromAccountNode(accountNode);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	    return account;
	}
	
	private Account createAccountFromAccountNode(final Element accountNode) throws Exception {
		AccountFactory factory = new AccountFactory();
		Account account = null;
		if(accountNode != null){
			account = factory.createAccount(accountNode.getAttributeValue("type"), accountNode.getAttributeValue("id"), accountNode.getChildText("firstName"), accountNode.getChildText("lastName"), accountNode.getChildText("sex"), accountNode.getChildText("currentTown"), accountNode.getChildText("mail"), accountNode.getChildText("passwd"));
			account.setAccountState(accountNode.getChildText("accountState"));
		}
		return account;
	}

	public boolean addAccount(Account account) {
		if(this.xmlAccountDAO.getAccountElement(account.getUserMail(), account.getPasswd()) == null){
			try{
				this.xmlAccountDAO.addAccountElement(account);
				return true;
			}
			catch(Exception e){
				return false;
			}
		}
		return false;
	}
	
	public boolean updateAccount(Account account) {
		try{
			this.xmlAccountDAO.updateAccountElement(account);
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean removeAccount(Account account) {
		try{
			this.xmlAccountDAO.removeAccountElement(account.getAccountID());
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
}
