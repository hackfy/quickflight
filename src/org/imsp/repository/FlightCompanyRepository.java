package org.imsp.repository;

import java.util.List;

import org.imsp.flight.FlightCompany;

public interface FlightCompanyRepository {
	public List<FlightCompany> getAllFlightCompanies();
	public FlightCompany getFlightCompany(String mail, String passwd);
	public boolean addFlightCompany(FlightCompany company);
	public boolean updateFlightCompany(FlightCompany company);
	public boolean removeFlightCompany(FlightCompany company);
}
