package org.imsp.repository;

import java.util.List;
import org.imsp.account.*;

public interface AccountRepository {
	public List<Account> getAllAccounts();
	public Account getAccount(String mail, String passwd);
	public boolean addAccount(Account account);
	public boolean updateAccount(Account account);
	public boolean removeAccount(Account account);
}
