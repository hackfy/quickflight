package org.imsp.repository;

import java.util.ArrayList;
import java.util.List;

import org.imsp.dao.XmlFlightCompanyDAO;
import org.imsp.flight.FlightCompany;
import org.jdom.Element;


public class XmlFlightCompanyRepository implements FlightCompanyRepository{
	private XmlFlightCompanyDAO xmlFlightCompanyDAO;

	public XmlFlightCompanyRepository(XmlFlightCompanyDAO xmlFlightCompanyDAO) {
	    this.xmlFlightCompanyDAO = xmlFlightCompanyDAO;
	}
	
	public List<FlightCompany> getAllFlightCompanies(){
		List<FlightCompany> companies = new ArrayList<FlightCompany>();
		List<Element> list = this.xmlFlightCompanyDAO.getAllFlightCompaniesElements();
		for(Element companyNode:list){
			try {
				companies.add(this.createFlightCompanyFromFlightCompanyNode(companyNode));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return companies;
	}
	
	public FlightCompany getFlightCompany(String mail, String passwd){
		Element companyNode = this.xmlFlightCompanyDAO.getFlightCompanyElement(mail, passwd);
		FlightCompany company = null;
		try {
			company = createFlightCompanyFromFlightCompanyNode(companyNode);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	    return company;
	}
	
	private FlightCompany createFlightCompanyFromFlightCompanyNode(final Element companyNode) throws Exception {
		FlightCompany company = null;
		if(companyNode != null){
			company = new FlightCompany(companyNode.getAttributeValue("id"), companyNode.getChildText("name"), companyNode.getChildText("mail"), companyNode.getChildText("passwd"));
		}
		return company;
	}
	
	public boolean addFlightCompany(FlightCompany company) {
		if(this.xmlFlightCompanyDAO.getFlightCompanyElement(company.getCompanyMail(), company.getCompanyPasswd()) == null){
			try{
				this.xmlFlightCompanyDAO.addFlightCompanyElement(company);
				return true;
			}
			catch(Exception e){
				return false;
			}
		}
		return false;
	}
	
	public boolean updateFlightCompany(FlightCompany company) {
		try{
			this.xmlFlightCompanyDAO.updateAccountElement(company);
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean removeFlightCompany(FlightCompany company) {
		try{
			this.xmlFlightCompanyDAO.removeFlightCompanyElement(company.getCompanyID());
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
}
