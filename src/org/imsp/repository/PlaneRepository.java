package org.imsp.repository;

import java.util.List;
import org.imsp.flight.Plane;

public interface PlaneRepository {
	public List<Plane> getAllPlanes();
	public Plane getPlane(String id);
	public boolean addPlane(Plane plane);
	public boolean updatePlane(Plane plane);
	public boolean removePlane(Plane plane);
}
