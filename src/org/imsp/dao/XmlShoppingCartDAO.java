package org.imsp.dao;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class XmlShoppingCartDAO{
	private File xmlFile;
	private SAXBuilder builder;
	
	public XmlShoppingCartDAO(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
	}
	
	public List<Element> getAllElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("shoppingCart");
		return list;
	}
	
	public Element getElement(String account){
		List<Element> shoppingList = this.getAllElements();
		for(int i=0; i< shoppingList.size(); i++){
			if(!shoppingList.get(i).getChildText("account").equals(account)){
				shoppingList.remove(i);
				i--;
			}
		}
		return shoppingList.size() == 1? shoppingList.get(0):null;
	}
}