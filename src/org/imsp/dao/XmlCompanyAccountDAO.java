package org.imsp.dao;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;


public class XmlCompanyAccountDAO{
	private File xmlFile;
	@SuppressWarnings("unused")
	private String fileName;
	private SAXBuilder builder;
	
	public XmlCompanyAccountDAO(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.fileName = xmlFileName;
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.fileName = xmlFileName;
	}
	
	public List<Element> getAllElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("company");
		return list;
	}
	
	
	public Element getElement(String mail, String passwd){
		List<Element> allAccounts = getAllElements();
	    for(int i=0; i < allAccounts.size(); i++) {
	    	Element node = allAccounts.get(i);
	    	if (!mail.equalsIgnoreCase(node.getChildText("mail")) || !passwd.equals(node.getChildText("password"))) {
	    		allAccounts.remove(i);
	    		i--;
	    	}
	    }
	    return allAccounts.size() == 1?allAccounts.get(0):null;
	}
	
}
	