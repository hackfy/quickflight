package org.imsp.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.imsp.account.Account;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;


public class XmlAccountDAO{
	private File xmlFile;
	private String fileName;
	private SAXBuilder builder;
	
	public XmlAccountDAO(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.fileName = xmlFileName;
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
	}
	
	public List<Element> getAllAccountElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("account");
		return list;
	}
	
	
	public Element getAccountElement(String mail, String passwd){
		List<Element> allAccounts = this.getAllAccountElements();
	    for(int i=0; i < allAccounts.size(); i++) {
	    	Element node = allAccounts.get(i);
	    	if (!mail.equalsIgnoreCase(node.getChildText("mail")) || !passwd.equals(node.getChildText("passwd"))) {
	    		allAccounts.remove(i);
	    		i--;
	    	}
	    }
	    return allAccounts.size() == 1?allAccounts.get(0):null;
	}
	
	public void addAccountElement(Account account){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		Element accountNode = new Element("account");
		Element firstName = new Element("firstName");
		firstName.setText(account.getFirstName());
		Element lastName = new Element("lastName");
		lastName.setText(account.getLastName());
		Element userSex = new Element("sex");
		userSex.setText(account.getSex());
		Element currentTown = new Element("currentTown");
		currentTown.setText(account.getCurrentTown());
		Element email=new Element("mail");
		email.setText(account.getUserMail());
		Element password = new Element("passwd");
		password.setText(account.getPasswd());
		Element state = new Element("accountState");
		state.setText(account.getAccountState());
		accountNode.addContent(firstName);
		accountNode.addContent(lastName);
		accountNode.addContent(userSex);
		accountNode.addContent(currentTown);
		accountNode.addContent(email);
		accountNode.addContent(password);
		accountNode.addContent(state);
		accountNode.setAttribute("id", account.getAccountID());
		accountNode.setAttribute("type", account.getType());
		rootNode.addContent(accountNode);
		try{
			XMLOutputter output0 = new XMLOutputter(Format.getPrettyFormat());
			output0.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
	}
	
	private boolean updateAccountNode(List<Element> list, Account account) {
		for(Element accountElement:list) {
			if(account.getAccountID().equals(accountElement.getAttributeValue("id"))){
				accountElement.getChild("firstName").setText(account.getFirstName());
				accountElement.getChild("lastName").setText(account.getLastName());
				accountElement.getChild("sex").setText(account.getSex());
				accountElement.getChild("currentTown").setText(account.getCurrentTown());
				accountElement.getChild("mail").setText(account.getUserMail());
				accountElement.getChild("passwd").setText(account.getPasswd());
				accountElement.getChild("accountState").setText(account.getAccountState());
				return true;
			}
	    }
		return false;
	}
	
	public boolean updateAccountElement(Account account){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren();
		boolean hasBeenFound = this.updateAccountNode(list, account);
		if(!hasBeenFound){
			return false;
		}
		
		XMLOutputter xmlOutputter = new XMLOutputter();
	    xmlOutputter.setFormat(Format.getPrettyFormat());
	    try{
	      xmlOutputter.output(document, new FileWriter(fileName));
	    }
	    catch(IOException ex) {
	      return false;
	    }
	    return true;
	}
	
	
	
	public void removeAccountElement(String id){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch (JDOMException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		@SuppressWarnings("unchecked")
		List<Element> accounts = document.getRootElement().getChildren("account");
		Iterator<Element> i = accounts.iterator();
		
		while(i.hasNext()){
			Element account = (Element)i.next();
			if(account.getAttributeValue("id").equals(id)){
				accounts.remove(account);
				break;
			}
		}
		
		try{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e){
			e.getStackTrace();
		}
	}
	
}
	