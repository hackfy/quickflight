package org.imsp.test;

import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.service.FlightServicesInterface;
import org.imsp.service.implementation.FlightServices;

public class Stories2and3and14and16and22 {
	public static void main(String[] args) {
		FlightServicesInterface service = new FlightServices();
		
		String departure = "Paris";//
		String arrival = "Cotonou";
		String queryDate = "2019-04-01, 00:00";
		String category = null; //Economic ou Regular ou Business
		
		List<Flight> flights = service.searchFlights(departure, arrival, queryDate, category);
		if(flights.size() == 0)
			System.out.println("Aucun vol disponible pour ces critères !");
		for(Flight flight:flights){
			if(category != null)
				System.out.println(flight.getFlightID() + "; places disponibles(classe " + category.toLowerCase()+ ") : " + (flight.getNumberOfSeatsByClass(category)-flight.getNumberOfReservedSeatsByClass(category)) + "; date du vol : " + flight.getFlightDepartureDate());
			else
				System.out.println(flight.getFlightID() + "; places disponibles : " + (flight.getNumberOfSeats()-flight.getNumberOfReservedSeats()) + "; date du vol : " + flight.getFlightDepartureDate());
		}
	}
}