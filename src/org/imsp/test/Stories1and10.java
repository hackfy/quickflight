package org.imsp.test;

import org.imsp.businessdomain.account.Account;
import org.imsp.service.implementation.AdminLogin;
import org.imsp.service.implementation.CustomerLogin;


public class Stories1and10 {

	public static void main(String[] args) {
		CustomerLogin service = new CustomerLogin();
		
		String cmail = "larry.michelson@outlook.com";
		String cpasswd = "larry@pass";
		
		Account account = service.login(cmail, cpasswd);
		if(!account.getType().equalsIgnoreCase("null"))
			System.out.println(account.getFirstName() + ", vous êtes connecté !");
		else
			System.out.println("Erreur ! Vérifiez les identifiants et reéssayez !");
		
		Account new_account = service.createAccount("cust001", "Larry", "Michelson", "M", "New-York", "larry.michelson@outlook.com", "larry@pass");
		if(new_account != null)
			System.out.println(new_account.getFirstName() + ", votre compte client a été créé !");
		else
			System.out.println("Erreur ! Un compte avec le même email existe déjà !");
		
		service.logout(account);
		if(account.getAccountID() == null){
			System.out.println("Vous êtes déconnecté(e) avec succès !");
		}
		
		AdminLogin serviceAdmin = new AdminLogin();
		String amail = "gedeon.hounkpatin@gmail.com";
		String apasswd = "hg2on@passwd";
		
		Account adminAccount = serviceAdmin.login(amail, apasswd);
		if(!adminAccount.getType().equalsIgnoreCase("null"))
			System.out.println(adminAccount.getFirstName() + ", vous êtes connecté en tant qu'administrateur !");
		else
			System.out.println("Erreur ! Vérifiez les identifiants et reéssayez !");
		
		serviceAdmin.logout(adminAccount);
		if(adminAccount.getAccountID() == null){
			System.out.println("Vous êtes déconnecté(e) avec succès !");
		}
	}

}
