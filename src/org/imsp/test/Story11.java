package org.imsp.test;

import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.service.AirLourdManagerInterface;
import org.imsp.service.implementation.AirLourdManager;


public class Story11 {

	public static void main(String[] args) {
		AirLourdManagerInterface manager = new AirLourdManager();
		String airlourdid = "IN0001";
		double weight = 20.0;
		
		List<Flight> airlourds = manager.getAllAirLourd();
		if(airlourds.size() == 0)
			System.out.println("Aucun vol AirLourd dans le système !");
		for(Flight flight:airlourds){
			System.out.println(flight + " - " + flight.getFlightID());
		}
		
		Flight airlourd = manager.getSpecificAirLourd(airlourdid);
		if(airlourd != null){
			double res = manager.setAirLourdExceedingWeight(airlourd, weight);
			if(res == airlourd.getFlightExceedingWeight())
				System.out.println("\nCharge excedentaire mise à jour !");
			else
				System.out.println("\nErreur de mise à jour !");
		}
	}

}
