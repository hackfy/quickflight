package org.imsp.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.FlightCompany;
import org.imsp.businessdomain.flight.Plane;
import org.imsp.service.FlightCompanyLoginInterface;
import org.imsp.service.FlightManagerInterface;
import org.imsp.service.PlaneManagerInterface;
import org.imsp.service.implementation.FlightCompanyLogin;
import org.imsp.service.implementation.FlightManager;
import org.imsp.service.implementation.PlaneManager;


public class Story23 {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
	public static void main(String[] args) {
		FlightCompanyLoginInterface service = new FlightCompanyLogin();
		
		String mail = "senegal@gmail.com";
		String passwd = "senegalpass";
		
		FlightCompany company = service.login(mail, passwd);
		if(company == null)
			System.out.println("Erreur de connexion !");
		else{
			System.out.println(company.getCompanyName() + ", bienvenue sur QuickFlight !");
			
			FlightManagerInterface flightmanager = new FlightManager();
			Flight flight = null;
			
			PlaneManagerInterface planemanager = new PlaneManager();
			Plane plane = planemanager.getAllPlanes(company.getCompanyID()).get(0);
			
			try {
				flight = flightmanager.addFlight("AirMoyen", "IN0006", company.getCompanyID(), plane.getPlaneID(), 42.0, "Paris", "Lomé", DATE_FORMAT.parse("2019-04-16, 15:15"));
			} 
			catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(flight != null)
				System.out.println(flight);
			else
				System.out.println("Erreur ! Le vol existe peut être déjà !");
		}
	}

}
