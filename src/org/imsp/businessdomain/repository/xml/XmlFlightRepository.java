package org.imsp.businessdomain.repository.xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.FlightFactory;
import org.imsp.businessdomain.flight.SeatClass;
import org.imsp.businessdomain.repository.FlightRepository;
import org.imsp.persistance.dao.xml.XmlFlightDAO;
import org.jdom.Element;

public class XmlFlightRepository implements FlightRepository{
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
	private XmlFlightDAO xmlFlightDAO;

	public XmlFlightRepository() {
	    this.xmlFlightDAO = new XmlFlightDAO();
	}
	public XmlFlightRepository(XmlFlightDAO xmlFlightDAO) {
	    this.xmlFlightDAO = xmlFlightDAO;
	}
	
	public List<Flight> getAllFlights(){
		List<Flight> flights = new ArrayList<Flight>();
		List<Element> list = this.xmlFlightDAO.getAllFlightElements();
		for(Element flightNode:list){
			try {
				flights.add(this.createFlightFromFlightNode(flightNode));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flights;
	}
	
	public List<Flight> getSpecificFlights(String departure, String arrival, String queryDate){
		List<Flight> flights = new ArrayList<Flight>();
		List<Element> list = this.xmlFlightDAO.getSpecificFlightElements(departure, arrival);
		
		if(queryDate == null){
			queryDate = DATE_FORMAT.format(new Date());
		}
		
		for(Element flightNode:list){
			String flightDate = this.getFlightDateFromFlightNode(flightNode);
			try{	
				if (isSameOrAfterDate(flightDate, queryDate))
					flights.add(this.createFlightFromFlightNode(flightNode));
			}
			catch (Exception e) {
					e.printStackTrace();
			}
		}
	    return flights;
	}
	
	public Flight getFlight(String flight_id){
		Element flightNode = this.xmlFlightDAO.getSpecificFlightElement(flight_id);
		Flight flight = null;
		if(flightNode != null) {
			try {
					flight = this.createFlightFromFlightNode(flightNode);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    return flight;
	}
	
	private Flight createFlightFromFlightNode(final Element flightNode) throws Exception {
		FlightFactory factory = new FlightFactory();
		Flight flight = factory.createFlight(flightNode.getAttributeValue("type"), flightNode.getAttributeValue("id"), flightNode.getChildText("company"), flightNode.getChildText("plane"), Double.parseDouble(flightNode.getChildText("maxWeight")), flightNode.getChildText("departureTown"), flightNode.getChildText("arrivalTown"), DATE_FORMAT.parse(flightNode.getChildText("departureDate")));
		if(flight.getName().equalsIgnoreCase("AirLourd")){
			flight.setFlightExceedingWeight(Double.parseDouble(flightNode.getChildText("exceedingWeight")));
		}
		
		int i = 0;
		@SuppressWarnings("unchecked")
		List<Element> seats = flightNode.getChild("seatsClass").getChildren();
		for(SeatClass seat:flight.getFlightSeatsClass()){
			if(seat.getSeatClassName().equals(seats.get(i).getAttributeValue("type"))){
				seat.setNumberOfSeats(Integer.parseInt(seats.get(i).getChildText("numberOfSeats")));
				seat.setNumberOfReservedSeats(Integer.parseInt(seats.get(i).getChildText("numberOfReservedSeats")));
				seat.setSeatPrice(Double.parseDouble(seats.get(i).getChildText("seatPrice")));
				i++;
			}
		}
		return flight;
	}
	
	public boolean addFlight(Flight flight) {
		if(this.xmlFlightDAO.getSpecificFlightElement(flight.getFlightID()) == null){
			try{
				this.xmlFlightDAO.addFlightElement(flight);
				return true;
			}
			catch(Exception e){
				return false;
			}
		}
		return false;
	}
	
	private boolean isSameOrAfterDate(String flightDate, String queryDate) {
		boolean value = false;
		try {
			value = DATE_FORMAT.parse(flightDate).getTime() >= DATE_FORMAT.parse(queryDate).getTime();
		} 
	    catch (ParseException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public String getFlightDateFromFlightNode(final Element flightNode){
	    try {
			return flightNode.getChildText("departureDate");
		}
	    catch (Exception e) {
			return DATE_FORMAT.format(new Date());
	    }
	}
	
	public boolean updateFlight(Flight flight) {
		try{
			this.xmlFlightDAO.updateFlightElement(flight);
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean removeFlight(Flight flight) {
		try{
			this.xmlFlightDAO.removeFlightElement(flight.getFlightID());
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
}
