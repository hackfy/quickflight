package org.imsp.businessdomain.repository.xml;

import java.util.ArrayList;
import java.util.List;

import org.imsp.businessdomain.flight.Plane;
import org.imsp.businessdomain.repository.PlaneRepository;
import org.imsp.persistance.dao.xml.XmlPlaneDAO;
import org.jdom.Element;

public class XmlPlaneRepository implements PlaneRepository{
	private XmlPlaneDAO xmlPlaneDAO;

	public XmlPlaneRepository() {
	    this.xmlPlaneDAO = new XmlPlaneDAO();
	}
	public XmlPlaneRepository(XmlPlaneDAO xmlPlaneDAO) {
	    this.xmlPlaneDAO = xmlPlaneDAO;
	}
	
	public List<Plane> getAllPlanes(){
		List<Plane> planes = new ArrayList<Plane>();
		List<Element> list = this.xmlPlaneDAO.getAllPlaneElements();
		for(Element planeNode:list){
			try {
				planes.add(this.createPlaneFromPlaneNode(planeNode));
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return planes;
	}
	
	public Plane getPlane(String id){
		Element planeNode = this.xmlPlaneDAO.getPlaneElement(id);
		Plane plane = null;
		try {
			plane = this.createPlaneFromPlaneNode(planeNode);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	    return plane;
	}
	
	private Plane createPlaneFromPlaneNode(final Element planeNode) throws Exception {
		@SuppressWarnings("unchecked")
		List<Element> seats = planeNode.getChild("seatsClass").getChildren();
		String[] seats_categories = new String[seats.size()];
		int[] seats_number = new int[seats.size()];
		for(int i=0; i<seats.size(); i++){
			seats_categories[i] = new String(seats.get(i).getAttributeValue("type"));
			seats_number[i] = Integer.parseInt(seats.get(i).getChildText("numberOfSeats"));
		}
		Plane plane = new Plane(planeNode.getAttributeValue("id"), planeNode.getChildText("model"), planeNode.getChildText("company"), seats_categories, seats_number);
		return plane;
	}

	public boolean addPlane(Plane plane) {
		if(this.xmlPlaneDAO.getPlaneElement(plane.getPlaneID()) == null){
			try{
				this.xmlPlaneDAO.addPlaneElement(plane);
				return true;
			}
			catch(Exception e){
				return false;
			}
		}
		return false;
	}
	
	public boolean updatePlane(Plane plane) {
		try{
			this.xmlPlaneDAO.updatePlaneElement(plane);
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public boolean removePlane(Plane plane) {
		try{
			this.xmlPlaneDAO.removePlaneElement(plane.getPlaneID());
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
}