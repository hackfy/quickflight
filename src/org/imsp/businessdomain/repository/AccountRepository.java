package org.imsp.businessdomain.repository;

import java.util.List;
import org.imsp.businessdomain.account.Account;

public interface AccountRepository {
	public List<Account> getAllAccounts();
	public Account getAccount(String mail, String passwd);
	public boolean addAccount(Account account);
	public boolean updateAccount(Account account);
	public boolean removeAccount(Account account);
}
