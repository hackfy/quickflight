package org.imsp.businessdomain.repository;

import java.util.List;

import org.imsp.businessdomain.flight.FlightCompany;


public interface FlightCompanyRepository {
	public List<FlightCompany> getAllFlightCompanies();
	public FlightCompany getFlightCompany(String mail, String passwd);
	public FlightCompany getFlightCompany(String id);
	public boolean addFlightCompany(FlightCompany company);
	public boolean updateFlightCompany(FlightCompany company);
	public boolean removeFlightCompany(FlightCompany company);
}
