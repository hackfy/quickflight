package org.imsp.businessdomain.repository;


import java.util.List;

import org.imsp.businessdomain.flight.Flight;


public interface FlightRepository {
	public List<Flight> getAllFlights();
	public List<Flight> getSpecificFlights(String departure, String arrival, String queryDate);
	public Flight getFlight(String flight_id);
	public boolean addFlight(Flight flight);
	public boolean updateFlight(Flight flight);
	public boolean removeFlight(Flight flight);
}