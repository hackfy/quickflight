package org.imsp.businessdomain.account;

import org.imsp.businessdomain.repository.AccountRepository;
import org.imsp.businessdomain.repository.xml.XmlAccountRepository;

public abstract class Account {
    protected String accountID;
	protected String firstName;
	protected String lastName;
	protected String sex;
	protected String currentTown;
	protected String mail;
	protected String passwd;
	protected String accountState;
    
	public Account(){
		this.accountID = null;
		this.firstName = null;
		this.lastName = null;
		this.sex = null;
		this.currentTown = null;
		this.mail = null;
		this.passwd = null;
		this.accountState = null;
	}
	
	public Account(String id, String first_name, String last_name, String sex, String current_town, String mail, String passwd){
		this.accountID = id;
		this.firstName = first_name;
		this.lastName = last_name;
		this.sex = sex;
		this.currentTown = current_town;
		this.mail = mail;
		this.passwd = passwd;
		this.setAccountState();
	}

	public String getUserMail() {
		return this.mail;
	}
	public String getAccountID(){
		return this.accountID;
	}
	public String getPasswd(){
		return this.passwd;
	}
	
	public void setUserMail(String mail) {
		this.mail = mail;
	}

	public void setUserPasswd(String passwd) {
		this.passwd = passwd;
	}


	public abstract String getType();

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCurrentTown() {
		return currentTown;
	}

	public void setCurrentTown(String currentTown) {
		this.currentTown = currentTown;
	}
	
	public String getAccountState() {
		return this.accountState;
	}
	public void setAccountState(String state) {
		this.accountState = state;
	}

	protected void setAccountState() {
		this.accountState = this.isCorrectMail(this.mail)? "actived":"disactived";
	}
	
	protected boolean isCorrectMail(String mail){
		return true;
	}
	
	public void destructAccount(){
		this.accountID = null;
		this.setFirstName(null);
		this.setLastName(null);
		this.setSex(null);
		this.setUserMail(null);
		this.setUserPasswd(null);
	}
	
	
	public boolean saveAccount(){
		AccountRepository repository = new XmlAccountRepository();
		return repository.addAccount(this);
	}
	
	public boolean updateAccount(){
		AccountRepository repository = new XmlAccountRepository();
		return repository.updateAccount(this);
	}
	
	public boolean removeAccount(){
		AccountRepository repository = new XmlAccountRepository();
		return repository.removeAccount(this);
	}
	
}

class AdminAccount extends Account {
	public AdminAccount(String id, String first_name, String last_name, String sex, String current_town, String mail, String passwd){
		super(id, first_name, last_name, sex, current_town, mail, passwd);
}

	public String getType() {
		return new String("admin");
	}
    
}

class CustomerAccount extends Account {	
	public CustomerAccount(String id, String first_name, String last_name, String sex, String current_town, String mail, String passwd){
		super(id, first_name, last_name, sex, current_town, mail, passwd);
	}

	public void subscribeToFlyCompany(Object flyCompany){
    }
	
	public String getType() {
		return new String("customer");
	}
}

class NullAccount extends Account {
	public NullAccount(){
		super();
	}
	public String getType() {
		return new String("null");
	}
}