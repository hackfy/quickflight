package org.imsp.businessdomain.flight;

public class SeatClass {
	protected String className;
	protected double seatPrice;
	protected int numberOfSeats;
	protected int numberOfReservedSeats;
	
	public SeatClass(String class_name, int number_of_seats){
		this.className = class_name;
		this.numberOfSeats = number_of_seats;
		this.numberOfReservedSeats = 0;
		this.seatPrice = 0;
	}
	
	public String getSeatClassName(){
		return this.className;
	}
	public void setSeatClassName(String className){
		this.className = className;
	}
	
	public int getNumberOfSeats(){
		return this.numberOfSeats;
	}
	public void setNumberOfSeats(int value){
		if(value <= 0){
			System.out.println("Bad value ! Must be greater or equal to 1");
		}
		else{
			this.numberOfSeats = value;
		}
	}
	
	public int getNumberOfReservedSeats(){
		return this.numberOfReservedSeats;
	}
	public void setNumberOfReservedSeats(int value){
		if(value < 0){
			System.out.println("Bad value ! Must be positive number");
		}
		else if(this.numberOfSeats - this.numberOfReservedSeats > value){
			this.numberOfReservedSeats = this.numberOfReservedSeats + value;
		}
		else{
			System.out.println("Bad value ! Must enter a value less than " + (this.numberOfSeats - this.numberOfReservedSeats)+ " !");
		}
	}
	
	public double getSeatPrice(){
		return this.seatPrice;
	}
	public void setSeatPrice(double value){
		if(value < 0){
			System.out.println("Bad value ! Must be positive value");
		}
		else{
			this.seatPrice = value;
		}
	}
	
}
