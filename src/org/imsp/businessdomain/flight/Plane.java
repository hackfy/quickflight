package org.imsp.businessdomain.flight;

import java.util.ArrayList;
import java.util.List;

import org.imsp.businessdomain.repository.PlaneRepository;
import org.imsp.businessdomain.repository.xml.XmlPlaneRepository;


public class Plane {
	private String planeID;
	private String planeModel;
	private String planeCompany;
	private List<SeatClass> seatsClass;
	
	public Plane(String plane_id, String plane_model, String company, String[] seats_categories){
		this.planeID = plane_id;
		this.planeModel = plane_model;
		this.planeCompany = company;
		this.seatsClass = new ArrayList<SeatClass>();
		for(String category: seats_categories){
			this.seatsClass.add(new SeatClass(category, 50));
		}
	}
	public Plane(String plane_id, String plane_model, String company, String[] categories, int[] seats_number){
		int i = 0;
		this.planeID = plane_id;
		this.planeModel = plane_model;
		this.planeCompany = company;
		this.seatsClass = new ArrayList<SeatClass>();
		for(String category:categories){
			this.seatsClass.add(new SeatClass(category, seats_number[i++]));
		}
	}
	
	public String getPlaneID(){
		return this.planeID;
	}
	public void setPlaneID(String value){
		this.planeID = value;
	}

	public String getPlaneModel(){
		return this.planeModel;
	}
	public void setPlaneModel(String value){
		this.planeModel = value;
	}
	
	public String getPlaneCompany(){
		return this.planeCompany;
	}
	public void setPlaneCompany(String value){
		this.planeCompany = value;
	}
	
	public List<SeatClass> getPlaneSeats(){
		return this.seatsClass;
	}
	
	public int getNumberOfSeatsByClass(String category){
		for(int i=0; i< this.seatsClass.size(); i++){
			if(this.seatsClass.get(i).getSeatClassName().compareToIgnoreCase(category) == 0){
				return this.seatsClass.get(i).getNumberOfSeats();
			}
		}
		return 0;
	}
	public void setNumberOfSeatsByClass(String category, int number_of_seat){
		for(int i=0; i< this.seatsClass.size(); i++){
			if(this.seatsClass.get(i).getSeatClassName().compareToIgnoreCase(category) == 0){
				this.seatsClass.get(i).setNumberOfSeats(number_of_seat);
				break;
			}
		}
	}
	
	public boolean savePlane(){
		PlaneRepository repository = new XmlPlaneRepository();
		return repository.addPlane(this);
	}
	
	public boolean updatePlane(){
		PlaneRepository repository = new XmlPlaneRepository();
		return repository.updatePlane(this);
	}
	
	public boolean removePlane(){
		PlaneRepository repository = new XmlPlaneRepository();
		return repository.removePlane(this);
	}
}
