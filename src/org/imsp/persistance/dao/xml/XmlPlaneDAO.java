package org.imsp.persistance.dao.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.imsp.businessdomain.flight.Plane;
import org.imsp.businessdomain.flight.SeatClass;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;


public class XmlPlaneDAO{
	private File xmlFile;
	private String fileName;
	private SAXBuilder builder;
	
	
	public XmlPlaneDAO(){
		this.fileName = "eclipse-workspace>/QuickFlight/src/org/imsp/persistance/xml/planes.xml";
		this.xmlFile = new File(this.fileName);
		this.builder = new SAXBuilder();
	}
	public XmlPlaneDAO(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.fileName = xmlFileName;
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
	}
	
	public List<Element> getAllPlaneElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("plane");
		return list;
	}
	
	
	public Element getPlaneElement(String id){
		List<Element> allPlanes = getAllPlaneElements();
	    for(int i=0; i < allPlanes.size(); i++) {
	    	Element node = allPlanes.get(i);
	    	if (!id.equalsIgnoreCase(node.getAttributeValue("id"))) {
	    		allPlanes.remove(i);
	    		i--;
	    	}
	    }
	    return allPlanes.size() == 1?allPlanes.get(0):null;
	}
	
	public void addPlaneElement(Plane plane){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		Element planeNode = new Element("plane");
		Element model = new Element("model");
		model.setText(plane.getPlaneModel());
		Element company = new Element("company");
		company.setText(plane.getPlaneCompany());
		Element seatsClass = new Element("seatsClass");
		for(SeatClass seat:plane.getPlaneSeats()){
			Element seatClass = new Element("seatClass");
			seatClass.setAttribute("type", seat.getSeatClassName());
			Element numberOfSeats = new Element("numberOfSeats");
			numberOfSeats.setText(String.valueOf(seat.getNumberOfSeats()));
			seatClass.addContent(numberOfSeats);
			seatsClass.addContent(seatClass);
		}
		planeNode.addContent(model);
		planeNode.addContent(company);
		planeNode.addContent(seatsClass);
		planeNode.setAttribute("id", plane.getPlaneID());
		rootNode.addContent(planeNode);
		try{
			XMLOutputter output0 = new XMLOutputter(Format.getPrettyFormat());
			output0.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
	}
	
	private boolean updatePlaneNode(List<Element> list, Plane plane) {
		for(Element planeElement:list) {
			if(plane.getPlaneID().equals(planeElement.getAttributeValue("id"))){
				planeElement.getChild("model").setText(plane.getPlaneModel());
				planeElement.getChild("company").setText(plane.getPlaneCompany());
				@SuppressWarnings("unchecked")
				List<Element> seats = planeElement.getChild("seatsClass").getChildren();
				int i = 0;
				for(SeatClass seat:plane.getPlaneSeats()){
					seats.get(i).getAttribute("type").setValue(seat.getSeatClassName());
					seats.get(i).getChild("numberOfSeats").setText(String.valueOf(seat.getNumberOfSeats()));
					i++;
				}
				return true;
			}
	    }
		return false;
	}
	
	public boolean updatePlaneElement(Plane plane){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren();
		boolean hasBeenFound = updatePlaneNode(list, plane);
		if(!hasBeenFound){
			return false;
		}
		
		XMLOutputter xmlOutputter = new XMLOutputter();
	    xmlOutputter.setFormat(Format.getPrettyFormat());
	    try{
	      xmlOutputter.output(document, new FileWriter(fileName));
	    }
	    catch(IOException ex) {
	      return false;
	    }
	    return true;
	}
	
	
	
	public void removePlaneElement(String id){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch (JDOMException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		@SuppressWarnings("unchecked")
		List<Element> planes = document.getRootElement().getChildren("plane");
		Iterator<Element> i = planes.iterator();
		
		while(i.hasNext()){
			Element plane = (Element)i.next();
			if(plane.getAttributeValue("id").equals(id)){
				planes.remove(plane);
				break;
			}
		}
		
		try{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e){
			e.getStackTrace();
		}
	}
	
}
	