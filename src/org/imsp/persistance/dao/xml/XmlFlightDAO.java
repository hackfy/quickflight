package org.imsp.persistance.dao.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.SeatClass;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XmlFlightDAO{
	private File xmlFile;
	private String fileName;
	private SAXBuilder builder;
	
	public XmlFlightDAO(){
		this.fileName = "eclipse-workspace>/QuickFlight/src/org/imsp/persistance/xml/flights.xml";
		this.xmlFile = new File(this.fileName);
		this.builder = new SAXBuilder();
	}
	public XmlFlightDAO(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
		this.fileName = xmlFileName;
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
	}
	
	public List<Element> getAllFlightElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("flight");
		return list;
	}
	
	
	public Element getSpecificFlightElement(String flightid){
		List<Element> allFlights = getAllFlightElements();
	    for(int i=0; i < allFlights.size(); i++) {
	    	Element node = allFlights.get(i);
	    	if(flightid.equalsIgnoreCase(node.getAttributeValue("id"))) {
	    		return node;
	    	}
	    }
	    return null;
	}
	
	public List<Element> getSpecificFlightElements(String departure, String arrival){
		List<Element> allFlights = getAllFlightElements();
	    for(int i=0; i < allFlights.size(); i++) {
	    	Element node = allFlights.get(i);
	    	try{
	    		if (!departure.equalsIgnoreCase(node.getChildText("departureTown")) || !arrival.equalsIgnoreCase(node.getChildText("arrivalTown"))) {
	    			allFlights.remove(i);
	    			i--;
	    		}
	    	}
	    	catch(NullPointerException e){
	    		if (!departure.equalsIgnoreCase(node.getChildText("departureTown"))) {
	    			allFlights.remove(i);
	    			i--;
	    		}
	    	}
	    }
	    return allFlights;
	}
	
	public void addFlightElement(Flight flight){
		Document document = new Document();
		XmlPlaneDAO planeDao = new XmlPlaneDAO();
		Element plane = planeDao.getPlaneElement(flight.getFlightPlane());
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		Element flightNode = new Element("flight");
		Element flightCompany = new Element("company");
		flightCompany.setText(flight.getFlightCompany());
		Element flightPlane = new Element("plane");
		flightPlane.setText(flight.getFlightPlane());
		Element maxWeight = new Element("maxWeight");
		maxWeight.setText(String.valueOf(flight.getFlightMaxWeight()));
		Element departureTown = new Element("departureTown");
		departureTown.setText(flight.getFlightDepartureTown());
		Element arrivalTown = new Element("arrivalTown");
		arrivalTown.setText(flight.getFlightArrivalTown());
		Element departureDate = new Element("departureDate");
		departureDate.setText(String.valueOf(flight.getFlightDepartureDate()));
		Element flightStatus = new Element("status");
		flightStatus.setText(flight.getFlightStatus());
		
		Element seatsClass = new Element("seatsClass");
		@SuppressWarnings("unchecked")
		List<Element> nodes = plane.getChild("seatsClass").getChildren();
		for(Element node:nodes){
			Element seatClass = new Element("seatClass");
			Element numberOfReservedSeats = new Element("numberOfReservedSeats");
			numberOfReservedSeats.setText("0");
			Element seatPrice = new Element("seatPrice");
			seatPrice.setText("0.0");
			@SuppressWarnings("unchecked")
			List<Element> children = node.getChildren();
			for(Element child:children){
				Element nnode = new Element(child.getName());
				nnode.setText(child.getText());
				seatClass.addContent(nnode);
			}
			seatClass.addContent(numberOfReservedSeats);
			seatClass.addContent(seatPrice);
			seatClass.setAttribute("type", node.getAttributeValue("type"));
			seatsClass.addContent(seatClass);
		}
		
		flightNode.addContent(flightCompany);
		flightNode.addContent(flightPlane);
		flightNode.addContent(maxWeight);
		if(flight.getName().equalsIgnoreCase("AirLourd")){
			Element exceedingWeight = new Element("exceedingWeight");
			exceedingWeight.setText(String.valueOf(flight.getFlightExceedingWeight()));
			flightNode.addContent(exceedingWeight);
		}
		flightNode.addContent(seatsClass);
		flightNode.addContent(departureTown);
		flightNode.addContent(arrivalTown);
		flightNode.addContent(departureDate);
		flightNode.addContent(flightStatus);
		flightNode.setAttribute("id", flight.getFlightID());
		flightNode.setAttribute("type", flight.getName());
		rootNode.addContent(flightNode);
		try{
			XMLOutputter output0 = new XMLOutputter(Format.getPrettyFormat());
			output0.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
	}
	
	private boolean updateFlightNode(List<Element> list, Flight flight) {
		for(Element flightElement:list) {
			if(flight.getFlightID().equals(flightElement.getAttributeValue("id"))){
				flightElement.getChild("company").setText(flight.getFlightCompany());
				flightElement.getChild("plane").setText(flight.getFlightPlane());
				flightElement.getChild("maxWeight").setText(String.valueOf(flight.getFlightMaxWeight()));
				if(flightElement.getAttributeValue("type").equalsIgnoreCase("AirLourd"))
					flightElement.getChild("exceedingWeight").setText(String.valueOf(flight.getFlightExceedingWeight()));
				flightElement.getChild("departureTown").setText(flight.getFlightDepartureTown());
				flightElement.getChild("arrivalTown").setText(flight.getFlightArrivalTown());
				flightElement.getChild("departureDate").setText(String.valueOf(flight.getFlightDepartureDate()));
				flightElement.getChild("status").setText(flight.getFlightStatus());
				
				@SuppressWarnings("unchecked")
				List<Element> seats = flightElement.getChild("seatsClass").getChildren();
				int i = 0;
				for(SeatClass seat:flight.getFlightSeatsClass()){
					if(seat.getSeatClassName().equals(seats.get(i).getAttributeValue("type"))){
						seats.get(i).getAttribute("type").setValue(seat.getSeatClassName());
						seats.get(i).getChild("numberOfSeats").setText(String.valueOf(seat.getNumberOfSeats()));
						seats.get(i).getChild("numberOfReservedSeats").setText(String.valueOf(seat.getNumberOfReservedSeats()));
						seats.get(i).getChild("seatPrice").setText(String.valueOf(seat.getSeatPrice()));
						i++;
					}
				}
				return true;
			}
	    }
		return false;
	}
	
	public boolean updateFlightElement(Flight flight){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren();
		boolean hasBeenFound = updateFlightNode(list, flight);
		if(!hasBeenFound){
			return false;
		}
		
		XMLOutputter xmlOutputter = new XMLOutputter();
	    xmlOutputter.setFormat(Format.getPrettyFormat());
	    try{
	      xmlOutputter.output(document, new FileWriter(fileName));
	    }
	    catch(IOException ex) {
	      return false;
	    }
	    return true;
	}
	
	public void removeFlightElement(String id){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch (JDOMException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		@SuppressWarnings("unchecked")
		List<Element> flights = document.getRootElement().getChildren("flight");
		Iterator<Element> i = flights.iterator();
		
		while(i.hasNext()){
			Element flight = (Element)i.next();
			if(flight.getAttributeValue("id").equals(id)){
				flights.remove(flight);
				break;
			}
		}
		
		try{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e){
			e.getStackTrace();
		}
	}
}
	