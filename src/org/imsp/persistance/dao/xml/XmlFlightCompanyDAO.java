package org.imsp.persistance.dao.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.imsp.businessdomain.flight.FlightCompany;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XmlFlightCompanyDAO {
	private File xmlFile;
	private String fileName;
	private SAXBuilder builder;
	
	public XmlFlightCompanyDAO(){
		this.fileName = "eclipse-workspace>/QuickFlight/src/org/imsp/persistance/xml/companies.xml";
		this.xmlFile = new File(this.fileName);
		this.builder = new SAXBuilder();
	}
	public XmlFlightCompanyDAO(String xmlFileName){
		this.fileName = xmlFileName;
		this.fileName = "eclipse-workspace>/QuickFlight/src/org/imsp/persistance/xml/companies.xml";
		this.xmlFile = new File(xmlFileName);
		this.builder = new SAXBuilder();
	}
	
	public void setXmlFile(String xmlFileName){
		this.xmlFile = new File(xmlFileName);
	}
	
	public List<Element> getAllFlightCompaniesElements(){
		Document document = new Document();
		try {
			document = builder.build(xmlFile);
		}
		catch (IOException io) {
		     System.out.println(io.getMessage());
		}
		catch (JDOMException jdomex) {
		     System.out.println(jdomex.getMessage());
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren("company");
		return list;
	}
	
	public Element getFlightCompanyElement(String mail, String passwd){
		List<Element> allCompanies = this.getAllFlightCompaniesElements();
	    for(int i=0; i < allCompanies.size(); i++) {
	    	Element node = allCompanies.get(i);
	    	if (!mail.equalsIgnoreCase(node.getChildText("mail")) || !passwd.equals(node.getChildText("passwd"))) {
	    		allCompanies.remove(i);
	    		i--;
	    	}
	    }
	    return allCompanies.size() == 1?allCompanies.get(0):null;
	}
	
	public Element getFlightCompanyElement(String id){
		List<Element> allCompanies = this.getAllFlightCompaniesElements();
	    for(int i=0; i < allCompanies.size(); i++) {
	    	Element node = allCompanies.get(i);
	    	if (!id.equals(node.getAttributeValue("id"))) {
	    		allCompanies.remove(i);
	    		i--;
	    	}
	    }
	    return allCompanies.size() == 1?allCompanies.get(0):null;
	}
	
	public void addFlightCompanyElement(FlightCompany company){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		Element companyNode = new Element("company");
		Element name = new Element("name");
		name.setText(company.getCompanyName());
		Element email=new Element("mail");
		email.setText(company.getCompanyMail());
		Element password = new Element("passwd");
		password.setText(company.getCompanyPasswd());
		companyNode.addContent(name);
		companyNode.addContent(email);
		companyNode.addContent(password);
		companyNode.setAttribute("id", company.getCompanyID());
		rootNode.addContent(companyNode);
		try{
			XMLOutputter output0 = new XMLOutputter(Format.getPrettyFormat());
			output0.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
	}
	
	private boolean updateFlightCompanyNode(List<Element> list, FlightCompany company) {
		for(Element companyElement:list) {
			if(company.getCompanyID().equals(companyElement.getAttributeValue("id"))){
				companyElement.getChild("name").setText(company.getCompanyName());
				companyElement.getChild("mail").setText(company.getCompanyMail());
				companyElement.getChild("passwd").setText(company.getCompanyPasswd());
				return true;
			}
	    }
		return false;
	}
	
	public boolean updateAccountElement(FlightCompany company){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch(JDOMException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Element rootNode = document.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> list = rootNode.getChildren();
		boolean hasBeenFound = this.updateFlightCompanyNode(list, company);
		if(!hasBeenFound){
			return false;
		}
		
		XMLOutputter xmlOutputter = new XMLOutputter();
	    xmlOutputter.setFormat(Format.getPrettyFormat());
	    try{
	      xmlOutputter.output(document, new FileWriter(fileName));
	    }
	    catch(IOException ex) {
	      return false;
	    }
	    return true;
	}
	
	public void removeFlightCompanyElement(String id){
		Document document = new Document();
		
		try {
			document = builder.build(xmlFile);
		} 
		catch (JDOMException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		@SuppressWarnings("unchecked")
		List<Element> companies = document.getRootElement().getChildren("company");
		Iterator<Element> i = companies.iterator();
		
		while(i.hasNext()){
			Element company = (Element)i.next();
			if(company.getAttributeValue("id").equals(id)){
				companies.remove(company);
				break;
			}
		}
		
		try{
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileOutputStream(fileName));
		}
		catch(Exception e){
			e.getStackTrace();
		}
	}
}
