package org.imsp.servlet;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.service.FlightManagerInterface;
import org.imsp.service.PlaneManagerInterface;
import org.imsp.service.implementation.FlightManager;
import org.imsp.service.implementation.PlaneManager;

public class AddFlightServlet extends HttpServlet {
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
    public static final String ATT_ERREURS  = "error";
    public static final String ATT_RESULTAT = "resultat";
    
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		PlaneManagerInterface planemanager = new PlaneManager();
		req.setAttribute( "companyid", session.getAttribute("companyid"));
		req.setAttribute( "planes", planemanager.getAllPlanes(session.getAttribute("companyid").toString()));
		req.setAttribute( "planemanager", planemanager);
		req.getRequestDispatcher("/WEB-INF/addfly.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		String flyid = req.getParameter("flyid");
		String company = req.getParameter("company");
		String planeid = req.getParameter("plane");
		Double maxWeight = Double.parseDouble(req.getParameter("maxWeight"));
		String departureTown = req.getParameter("departureTown");
		String arrivalTown = req.getParameter("arrivalTown");
		String date = req.getParameter("date");
		String time = req.getParameter("time");
		String depatureDate = date +','+' '+time;
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String now = dateFormat.format(actuelle);
		Date date1 = null;
		Date date2 = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //Pour déclarer les valeurs dans les nouveaux objets Date, employez le même format de date que pour créer des dates
		try {
			date1 = sdf.parse(now);
			date2 = sdf.parse(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
		} 	
		System.out.println("dededec"+date1);
		System.out.println("courant"+date2);
		FlightManagerInterface flightmanager = new FlightManager();
		Flight flight = null;
		if(date2.after(date1))
		{
		if(maxWeight<23.5)
		{
			try {
				flight = flightmanager.addFlight("AirLeger", flyid,company, planeid, maxWeight, departureTown, arrivalTown, DATE_FORMAT.parse(depatureDate));
			}
			catch(Exception e)
			{}
		}	
		else if(maxWeight>=23.5 && maxWeight<=42.0){
			try {
				flight = flightmanager.addFlight("AirMoyen", flyid,company, planeid, maxWeight, departureTown, arrivalTown, DATE_FORMAT.parse(depatureDate));
			}
			catch(Exception e)
			{}
		}
		else if(maxWeight>= 42.5 && maxWeight<=65.0){
			try {
				flight = flightmanager.addFlight("AirLourd", flyid, company, planeid, maxWeight, departureTown, arrivalTown, DATE_FORMAT.parse(depatureDate));
			}
			catch(Exception e)
			{}
				
		}
		}
		else if(date2.before(date1))
		{	
			flight = null;
		}
		String resultat = null;
		String error = null;
		if(flight != null)
			resultat = "Succès de l'ajout.";
		else
            error = "Échec de l'ajout";
		req.setAttribute(ATT_ERREURS, error);
		req.setAttribute( ATT_RESULTAT, resultat );
		this.getServletContext().getRequestDispatcher("/WEB-INF/addfly.jsp").forward(req, resp);			
	}
}