package org.imsp.servlet;
import org.imsp.businessdomain.account.Account;
import org.imsp.service.implementation.CustomerLogin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException{
		String email = req.getParameter("email");
		String password =req.getParameter("password");
		CustomerLogin service = new CustomerLogin();
        HttpSession session = req.getSession(true);
		try {		
		Account account = service.login(email, password);

        if(!account.getType().equalsIgnoreCase("null"))
				{
					session.setAttribute("Nom",account.getFirstName());
					session.setAttribute("Prenom",account.getLastName());
					session.setAttribute("typeUser",account.getType().toString());
					session.setAttribute("ville",account.getCurrentTown().toString());
					resp.sendRedirect("/QuickFlight/customer");
				}
		else 	{
					String error = "";
					error = "Mail ou mot de passe incorrect !";
					req.setAttribute("error", error);
					this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
				}
			}
		catch(Exception e)
		{	
			String error = "";
			error = "Mail ou mot de passe incorrect !";
			req.setAttribute("error", error);
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
		}
	}
}