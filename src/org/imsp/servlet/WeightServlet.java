package org.imsp.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.service.AirLourdManagerInterface;
import org.imsp.service.implementation.AirLourdManager;



public class WeightServlet extends HttpServlet {
    public static final String ATT_RESULTAT = "resultat";
    public static final String ATT_ERREURS  = "erreurs";

	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		req.getRequestDispatcher("/WEB-INF/manageweight.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException{
		String airlourdid = req.getParameter("airlourdid");
		String wg = req.getParameter("weight");
		Double weight =Double.parseDouble(wg);
		AirLourdManagerInterface manager = new AirLourdManager();
		Flight airlourd = manager.getSpecificAirLourd(airlourdid);
	    String resultat = null;
		if(airlourd != null){
			double res = manager.setAirLourdExceedingWeight(airlourd, weight);
			if(res == airlourd.getFlightExceedingWeight())
			{
	            resultat = "Charge excedentaire mise à jour !";
			}
			else {
	            resultat = "Erreur de mise à jour !";
			}
		}
		else {
			resultat = "Erreur de mise à jour !";
		}
			req.setAttribute("error", resultat);
			this.getServletContext().getRequestDispatcher("/WEB-INF/manageweight.jsp").forward(req, resp);
	}		
}