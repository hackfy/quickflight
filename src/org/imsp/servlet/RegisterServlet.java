package org.imsp.servlet;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imsp.businessdomain.account.Account;
import org.imsp.service.implementation.CustomerLogin;


public class RegisterServlet extends HttpServlet {
    public static final String ATT_ERREURS  = "erreurs";
    public static final String ATT_RESULTAT = "resultat";
    
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/register.jsp").forward(req, resp);;
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
	    UUID uuid = UUID.randomUUID();
		String id = uuid.toString();
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String sex = req.getParameter("sex");
		String email = req.getParameter("email");
		String town = req.getParameter("town");
		String password = req.getParameter("password");
		String rpassword = req.getParameter("rpassword");
		CustomerLogin service = new CustomerLogin();
	    String resultat = null;
	    Map<String, String> erreurs = new HashMap<String, String>();
		        try {
		            validationMotsDePasse( password, rpassword );
		        } catch ( Exception e ) {
		            erreurs.put( password, e.getMessage() );
		        }
				if( erreurs.isEmpty() ){
						Account new_account = service.createAccount(id, lastName, firstName, sex, town, email, password);
			        if(new_account != null) {
							resultat = "Succès de l'inscription.";
							this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
						}
					else{
						 erreurs.put( email,"existe deja");}
			        }
			    req.setAttribute( ATT_ERREURS, erreurs );
		        req.setAttribute( ATT_RESULTAT, resultat );
				this.getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(req, resp);
}
	private void validationMotsDePasse( String motDePasse, String confirmation ) throws Exception{
	    if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
	        if (!motDePasse.equals(confirmation)) {
	            throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
	        } else if (motDePasse.trim().length() < 3) {
	            throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
	        }
	    } else {
	        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
	    }
	}
}