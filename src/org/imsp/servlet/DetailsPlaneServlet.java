package org.imsp.servlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.businessdomain.flight.FlightCompany;
import org.imsp.service.FlightCompanyLoginInterface;
import org.imsp.service.FlightManagerInterface;
import org.imsp.service.implementation.FlightCompanyLogin;
import org.imsp.service.implementation.FlightManager;

public class DetailsPlaneServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		FlightCompanyLoginInterface service = new FlightCompanyLogin();
		String id= req.getParameter("id");
		FlightManagerInterface info = new FlightManager();
		Flight flight = info.getFlight(id);
		FlightCompany company = service.getFlightCompanyByID(flight.getFlightCompany());
		req.setAttribute( "flight", flight);
		req.setAttribute( "company", company.getCompanyName());
		service =null;
		req.getRequestDispatcher("/WEB-INF/detailsplane.jsp").forward(req, resp);
	
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
	
	
	
}