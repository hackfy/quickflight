package org.imsp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.imsp.businessdomain.account.Account;
import org.imsp.service.implementation.AdminLogin;


public class AdminServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.getRequestDispatcher("/WEB-INF/admin.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		String email = req.getParameter("email");
		String password =req.getParameter("password");
		AdminLogin serviceAdmin = new AdminLogin();
		HttpSession session = req.getSession(true);
		try {		
			Account adminAccount = serviceAdmin.login(email, password);
	        if(!adminAccount.getType().equalsIgnoreCase("null"))
					{
			        	session.setAttribute("Nom",adminAccount.getFirstName());
						session.setAttribute("Prenom",adminAccount.getLastName());
						session.setAttribute("typeUser",adminAccount.getType().toString());
						resp.sendRedirect("/QuickFlight/admindash");
					}
			else {
						String error = "";
						error = "Mail ou mot de passe incorrect !";
						req.setAttribute("error", error);
						this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(req, resp);
				}
				}
			catch(Exception e)
			{	
				String error = "";
				error = "Mail ou mot de passe incorrect !";
				req.setAttribute("error", error);
				this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(req, resp);
			}
		}
	}