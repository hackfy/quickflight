package org.imsp.servlet;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imsp.service.FlightCompanyLoginInterface;
import org.imsp.service.FlightServicesInterface;
import org.imsp.service.implementation.FlightCompanyLogin;
import org.imsp.service.implementation.FlightServices;


public class CustomerServlet extends HttpServlet {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd, HH:mm");
	public static final String ATT_ERREURS  = "erreurs";
	public static final String ATT_RESULTAT = "resultat";
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/customer.jsp").forward(req, resp);;
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		String from_city = req.getParameter("from_city");
		String to_city =req.getParameter("to_city");
		if(to_city.isEmpty())
			to_city=null;
		String  departure_date = req.getParameter("departure_date");
		if(departure_date.isEmpty())
			departure_date= DATE_FORMAT.format(new Date());
		else {
			Calendar cal = Calendar.getInstance();
	        int hour = cal.get(Calendar.HOUR_OF_DAY);
	        int minute = cal.get(Calendar.MINUTE);
	        departure_date = departure_date+','+' '+hour+':'+minute;
		}
		Double maxWeight ;
		String Vclass =req.getParameter("class");
		if(Vclass.isEmpty())
			Vclass = null;
		String weight = (req.getParameter("poids"));
		if(weight.isEmpty())
			maxWeight = 0.0;
		else
			maxWeight =Double.parseDouble(req.getParameter("poids"));
		FlightServicesInterface service = new FlightServices();
		FlightCompanyLoginInterface companys = new FlightCompanyLogin();
		req.setAttribute("companya", companys);
		req.setAttribute( "category", Vclass);
		String controlDate = departure_date;
		Date actuelle = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd, hh:mm");
		String now = dateFormat.format(actuelle);
		String nowtimeC= DATE_FORMAT.format(new Date());
		Date date1 = null;
		Date date2 = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd, hh:mm"); //Pour déclarer les valeurs dans les nouveaux objets Date, employez le même format de date que pour créer des dates
		try {
			date1 = sdf.parse(now);
			date2 = sdf.parse(controlDate);
		} catch (ParseException e1) {
			e1.printStackTrace();
		} 	
		if(date2.before(date1))
		{
		req.setAttribute("volsAB",service.searchFlightsWithWeight(from_city, to_city,nowtimeC,Vclass,maxWeight));
		if(to_city != null)
			req.setAttribute("volsBA",service.searchFlightsWithWeight(to_city, from_city,nowtimeC,Vclass,maxWeight));
		}
		else if(date2.after(date1))
		{
			req.setAttribute("volsAB",service.searchFlightsWithWeight(from_city, to_city,departure_date,Vclass,maxWeight));
		if(to_city != null)
			req.setAttribute("volsBA",service.searchFlightsWithWeight(to_city, from_city,departure_date,Vclass,maxWeight));
			
		}
		req.setAttribute("max",maxWeight);	
		req.setAttribute("push",from_city);	
		req.setAttribute("weight",weight);
		this.getServletContext().getRequestDispatcher("/WEB-INF/customer.jsp").forward(req, resp);
	}
}