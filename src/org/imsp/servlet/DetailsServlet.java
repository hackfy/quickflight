package org.imsp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imsp.businessdomain.flight.Flight;
import org.imsp.service.FlightManagerInterface;
import org.imsp.service.implementation.FlightManager;

public class DetailsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
			String id= req.getParameter("id");
			FlightManagerInterface info = new FlightManager();
			Flight flight = info.getFlight(id);
			req.setAttribute( "flight", flight);
			req.setAttribute( "company", flight.getFlightCompanyName());
			req.getRequestDispatcher("/WEB-INF/details.jsp").forward(req, resp);
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
	}
}