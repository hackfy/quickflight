package org.imsp.servlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.imsp.businessdomain.flight.FlightCompany;
import org.imsp.service.FlightCompanyLoginInterface;
import org.imsp.service.implementation.FlightCompanyLogin;

public class CompagnieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		req.getRequestDispatcher("/WEB-INF/company.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException{
		String email = req.getParameter("email");
		String password =req.getParameter("password");
		FlightCompanyLoginInterface service = new FlightCompanyLogin();
        HttpSession session = req.getSession(true);
		try {	
			FlightCompany company = service.login(email, password);
	        if(company != null){
		        	session.setAttribute("company",company.getCompanyName());
			        session.setAttribute("companyid",company.getCompanyID());
					session.setAttribute("typeUser","Company");
					resp.sendRedirect("/QuickFlight/companydash");
			}
			else {
						String error = "";
						error = "Mail ou mot de passe incorrect !";
						req.setAttribute("error", error);
						this.getServletContext().getRequestDispatcher("/WEB-INF/company.jsp").forward(req, resp);
						}
					}
				
			catch(Exception e)
			{	
				String error = "";
				error = "Mail ou mot de passe incorrect !";
				req.setAttribute("error", error);
				this.getServletContext().getRequestDispatcher("/WEB-INF/company.jsp").forward(req, resp);
			}
		}
	}