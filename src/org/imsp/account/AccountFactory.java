package org.imsp.account;

public class AccountFactory {
	public Account createAccount(String account_type, String id, String first_name, String last_name, String sex, String current_town, String mail, String passwd){
        Account account = new NullAccount();
		if(account_type.equalsIgnoreCase("admin"))
        	account = new AdminAccount(id, first_name, last_name, sex, current_town, mail, passwd);  
        else if(account_type.equalsIgnoreCase("customer"))
        	account = new CustomerAccount(id, first_name, last_name, sex, current_town, mail, passwd);     
		return account;
	}
}