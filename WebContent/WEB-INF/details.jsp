
<%@include file="header.jsp" %>        
        
      <div class="row">
        <div class="col-lg-12">
          <div class="well bs-component">
            <form class="form-horizontal" method="POST"> <!--Search Form -->
              <center>
              	<legend>Details Flights</legend>
              </center>
              <div class="form-group">
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">Type: </label>
         				 <input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getName()}" />" disabled>
                </div>
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">Company: </label>
						<input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${company}"/>" disabled>
	            </div>
              </div>
              <div class="form-group">
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">Departure Town:</label>
                  <input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getFlightDepartureTown()}"/>" disabled>
                </div>
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">Arrival Town :</label>
              		<input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getFlightArrivalTown()}" />" disabled>
                </div>
               <div class="col-lg-6">
                  <label for="select" class="control-label">Departure Date :</label>
                      <input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getFlightDepartureDate()}" />" disabled>
                  <br>
                </div>
                <div class="col-lg-6">
                  <label for="select" class="control-label">Status:</label>
                      <input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getFlightStatus()}" />" disabled>
                  <br>
                </div>
				<c:forEach var="category" items="${flight.getFlightSeatsClass()}">
	                    <div class="col-lg-6">
	                    <label for="select" class="control-label"> Place disponibles(<c:out value="${category.getSeatClassName()}" />) :</label>
	                    	<input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getNumberOfSeatsByClass(category.getSeatClassName())-flight.getNumberOfReservedSeatsByClass(category.getSeatClassName())}" />" disabled>
	                    </div>
	                    <div class="col-lg-6">
	                    <label for="select" class="control-label">Prix (<c:out value="${category.getSeatClassName()}" />)</label>
	         		  		   <input class="form-control" id="disabledInput" type="text" placeholder="<c:out value="${flight.getSeatPrice(category.getSeatClassName())}" />" disabled>
	         		  	</div>
	           </c:forEach>		           
              </div>
            </form>
                        	<strong><p style="text-align: center; color:white"><a href="/QuickFlight/index"> Retour � la page d'accueil</a></p></strong><br/><br/>
            
        </div>
    </div>
</div>        
        
       
        
        
<%@include file="footer.jsp" %>