<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jsp" %>
            <form class="form-horizontal"  method="POST">
              <div class="row">
                 <div class="col-lg-4">
            	    <div class="well bs-component">
	                  <legend>Company Log In</legend>
	                  <c:if test="${!empty error}"><p style="color:red;"><c:out value="${error}"/></p></c:if>
	                  <div class="form-group">
	                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
	                    <div class="col-lg-12">
	                      <input type="email" name="email" class="form-control" required placeholder="Email">
	                    </div>
	                 </div>
	                  <div class="form-group">
	                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-12">
	                      <input type="password" name="password" class="form-control" required placeholder="Password">
	                    </div>
	                  </div>
		            	<div class="form-group">
		                	<center><button type="submit" id="submit" value="submit" name="submit" class="btn btn-success">Login</button></center>
		                </div>
              </form>
<%@include file="footer.jsp" %>