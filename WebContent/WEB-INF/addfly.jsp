<% if ((session.getAttribute("typeUser") == null && !(session.getAttribute("typeUser") == "company"))) { %>
    <jsp:forward page="/index"></jsp:forward>
<% } %>

<%@include file="headercompany.jsp" %>
            <form class="form-horizontal" method="POST">
              <div class="row">
                 <div class="col-lg-4">
            	    <div class="well bs-component">
	                  <legend>Add Flight</legend>             			
             		<c:choose>
		 					<c:when test="${resultat != null}">
						<p class="alert alert-info alert-dismissible" role="alert" >
			                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    		<span aria-hidden="true">x</span></button><c:out value="${resultat}"/>
					    		</p> 
					    	</c:when>
            				<c:when test="${error != null}">
							<p class="alert alert-danger alert-dismissible" role="alert" >
			                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    		<span aria-hidden="true">x</span></button><c:out value="${error}"/>
			    	 		</p>            				
			    	 		</c:when>
           			  </c:choose>
             	
			    	<div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">FlyID</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="flyid" class="form-control" required placeholder="FlyID">
	                    </div>
	                 </div>
	                 <div class="form-group">
	                      <input type="hidden" name="company" value="<c:out value="${companyid}"/>">
	                 </div>
	                  <div class="form-group">
		                  <label for="inputText" class="col-lg-2 control-label">Plane</label>
		                  <div class="col-lg-12">
			              <select class="form-control" name="plane" id="select">
			              	<option name="" value="">Choisir un avion</option>
			                <c:forEach var="plane" items="${planes}">
								<option value="<c:out value="${plane.getPlaneID()}"/>"><c:out value="${plane.getPlaneID()}"/></option>
							</c:forEach>
			              </select>
			              </div>
                	 </div>	
	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">MaxWeight</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="maxWeight" class="form-control" required placeholder="maxWeight">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">DepartureTown</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="departureTown" class="form-control" required placeholder="departureTown">
	                    </div>	                    
	                 </div>
	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">ArrivalTown</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="arrivalTown" class="form-control" required placeholder="ArrivalTown">
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">DepatureDate</label>
	                    <div class="col-lg-12">
                 			 <input class="form-control"  name="date" id="datepicker" value="" required type="text">
	                    </div>
	                 </div>
		             	<div class="form-group">
		                    <label for="inputText" class="col-lg-2 control-label">Time</label>
		                    <div class="col-lg-12">
	                 			 <input class="form-control"  name="time" id="timepicker" value="" required type="text">
		                    </div>
		                 </div>	             		
		            	<div class="form-group">
		                	<center><button type="submit" id="submit" value="submit" name="submit" class="btn btn-success">Add</button></center>
		                </div>
              </form>
<%@include file="footer.jsp" %>