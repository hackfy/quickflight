<% if ((session.getAttribute("typeUser") == null && !(session.getAttribute("typeUser") == "company"))) { %>
    <jsp:forward page="/index"></jsp:forward>
<% } %>
<%@include file="headercompany.jsp" %>
            <form class="form-horizontal" method="POST">
              <div class="row">
              AirLourd info
             <table id="table_id" class="display compact cell-border" cellspacing="0"> 
		 		<thead>
                    <tr>
	                  		<th>flightID</th>
	                        <th>planeID</th>
	                        <th>Places disponibles</th>
	                        <th>Type d'appareil</th>
	                        <th>Poids</th>
	                        <th></th>
	                        <th></th>
                    </tr>
                </thead>
		 	<tbody>			  
		    <c:forEach var="vol" items="${volsAB}">
		      	   <tr>
		      	   
				      <td> <c:out value="${vol.getFlightID()}" /></td>
				 	  <td> <c:out value="${vol.getFlightPlane()}" /></td>
				 	  <c:choose>
		 					<c:when test="${category != null}">
		 						<td> <c:out value="${vol.getNumberOfSeatsByClass(category)-vol.getNumberOfReservedSeatsByClass(category)}" /></td>
            				</c:when>
            				<c:when test="${category == null}">
		 						<td> <c:out value="${vol.getNumberOfSeats()-vol.getNumberOfReservedSeats()}" /></td>
            				</c:when>
           			  </c:choose>
           			  <td><c:out value="${vol.getName()}" /></td>
				      <td><c:out value="${vol.getFlightMaxWeight()+vol.getFlightExceedingWeight()}" /></td>
				      <td><c:out value="${vol.getFlightArrivalTown()}" /></td>
				      
			      </tr>
		    </c:forEach>
		    </tbody>
		</table>
              
              
              
              </div>
              </form>
<%@include file="footer.jsp" %>