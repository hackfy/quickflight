<% if ((session.getAttribute("typeUser") == null && !(session.getAttribute("typeUser") == "company"))) { %>
    <jsp:forward page="/index"></jsp:forward>
<% } %>
<%@include file="headercompany.jsp" %>
              <form class="form-horizontal" method="POST">
              <div class="row">
                 <div class="col-lg-4">
            	    <div class="well bs-component">
	                  <legend>Configuration poids  AirLourd</legend>
					<c:if test="${!empty error}"><p class="alert alert-info alert-dismissible" role="alert" >
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    		</button><c:out value="${error}"/></p></c:if>	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">AirLourdID</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="airlourdid" class="form-control" required placeholder="airlourdid">
	                    </div>
	                 </div>
	                  <div class="form-group">
	                    <label for="inputText" class="col-lg-2 control-label">MAX</label>
	                    <div class="col-lg-12">
	                      <input type="text" name="weight" class="form-control" required placeholder="Poids">
	                    </div>
	                  </div>
		            	<div class="form-group">
		                	<center><button type="submit" id="submit" value="submit" name="submit" class="btn btn-success">Mise a jour</button></center>
		                </div>
              </form>
<%@include file="footer.jsp" %>