<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="header.jsp" %>
	<div class="row">
        <div class="col-lg-4">
          <div class="well bs-component">
			<form class="form-horizontal" action="" method="POST">
              <legend>User Registration</legend>
             <c:choose>
		 		<c:when test="${resultat != null}">
					<p class="alert alert-info alert-dismissible" role="alert" >
			          	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					   	<span aria-hidden="true">x</span></button><c:out value="${resultat}"/>
					</p> 
				</c:when>
            	<c:when test="${erreurs != null}">
					<p class="alert alert-danger alert-dismissible" role="alert" >
			               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    	<span aria-hidden="true">x</span></button><c:out value="${erreurs}"/>
			    	 </p>            				
			    </c:when>
           	</c:choose>       
			  <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">First Name</label>
                  <input class="form-control" name="firstName" required type="text" value="<c:out value="${param.firstName}"/>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">Last Name</label>
                  <input class="form-control" name="lastName" required type="text" value="<c:out value="${param.lastName}"/>">
                </div>
              </div>
               <div class="form-group">
                <div class="col-lg-10">
                  <div class="radio">
                    <label>
                      <input type="radio" name="sex" value="M" required />
                      Male
                    </label>
                    <label>
                      <input type="radio" name="sex" value="F" required />
                      Female
                    </label>
                  </div>      
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">Town</label>
                  <input class="form-control" name="town"  required type="text" value="<c:out value="${param.ville}"/>">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">Email</label>
                  <input class="form-control" type="email"  name="email"  required type="text" value="<c:out value="${param.email}"/>">
                  <span class="erreur">${erreurs['email']}</span> 
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">Password</label>
                  <input class="form-control" name="password" id="u_password" required type="password">
               	  <span class="erreur">${erreurs['motdepasse']}</span>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  <label class="control-label" for="focusedInput">Confirm Password</label>
                  <input class="form-control" name="rpassword" id="u_password_again" required type="password">
                   <span class="erreur">${erreurs['confirmation']}</span>
                </div>
              </div>
              <div class="form-group">
                <center><button type="submit" id="submit" value="submit" name="submit" class="btn btn-primary">Register</button></center>
            	<strong><a href="/QuickFlight/login" style="color:white">Login Now</a></strong><br/><br/>
					
					</div>
			</form>
			</div>
			</div>
			</div>
<%@include file="footer.jsp" %>
