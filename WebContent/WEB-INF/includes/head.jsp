<head>
    <meta charset="utf-8">
    <title>AirX Airlines</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="description" content="AirX is Flight Booking System">
  	<meta name="keywords" content="Flight,Air Ticket,AirX,Booking,Online">
  	<meta name="author" content="Vasu Garg">
    <link rel="stylesheet" href="vendor/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="vendor/css/bootswatch.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118294394-1"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
	jQuery(document).ready(function()
	{
	   jQuery('#toggle').hide();
	   jQuery('a#toggler').click(function()
	  {
	      jQuery('#toggle').toggle(400);
	      return false;
	   });
	});
</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-118294394-1');
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
   	<link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="vendor/js/bootstrap.min.js"></script>
    <script src="vendor/js/bootswatch.js"></script>
    <script type="text/javascript">
	    $( function() {
	        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
	      } );
	    $( function() {
	    $('#timepicker').timepicker({
            timeFormat: 'h:mm ',
                    });
   });
	
	    function afficher_cacher(id)
	    {
	        if(document.getElementById(id).style.visibility=="hidden")
	        {
	            document.getElementById(id).style.visibility="visible";
	        }
	        else
	        {
	            document.getElementById(id).style.visibility="hidden";
	        }
	        return true;
	    }   
	    $(document).ready(function() {
	    	var oTable = $('#table_id').dataTable( {
	    	"oLanguage": {
	    	"sEmptyTable": "Pas de vols disponible pour cette recherche"
	    	},
	    	});
	    	} );
	    $(document).ready(function() {
	    	var oTable = $('#table_id2').dataTable( {
	    	"oLanguage": {
	    	"sEmptyTable": "Pas de vols retour disponible pour cette recherche"
	    	},
	    	});
	    	} );
	    

	  </script>         
</head>