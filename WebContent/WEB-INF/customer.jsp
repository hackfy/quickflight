<% if ((session.getAttribute("typeUser") != null)) { %>
    <jsp:forward page="/index"></jsp:forward>
<% } %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="headercustomer.jsp" %>
<jsp:useBean id="now" class="java.util.Date" />
      <div class="row">
        <div class="col-lg-12">
          <div class="well bs-component">
            <form class="form-horizontal" method="POST"> <!--Search Form -->
              <center>
              	<legend>Search Flights</legend>
              </center>
              <div class="form-group">
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">From</label>
                  <input class="form-control" name="from_city"  value="<c:out value="${ sessionScope.ville}"/>" required type="text">
                </div>
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">To</label>
                  <input class="form-control" name="to_city" id="to_city" value=""  required type="text">
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-6">
                  <label class="control-label" for="focusedInput">Departure Date</label>
                  <fmt:formatDate var="fmtDate" value="<%=new java.util.Date()%>" pattern="dd/MM/yyyy"/>
                  <input class="form-control"  name="departure_date" id="datepicker" value="" required type="text">
                </div>
                <div class="col-lg-3">
                  <label class="control-label" for="focusedInput">Poids</label>
                  <input class="form-control" type="text" name="poids"  value="">
                </div>
               <div class="col-lg-3">
                  <label for="select" class="control-label">Class</label>
	                  <select class="form-control" name="class" id="select" >
	                    <option name="nullClass" value="">Entrer une categorie</option>
	                    <option name="economic" value="economic">Economique</option>
	                    <option name="regular" value="regular">Regulier</option>
	                    <option name="Business" value="Business">Affaire</option>
	                  </select>
                  <br>
                </div>
              </div>
              <div class="form-group">
                <center><button type="submit" name="submit"   class="btn btn-primary" id="bouton_texte">Rechercher vols</button></center>
              </div>
            </form>
        </div>
    </div>
</div>
 <% if(request.getAttribute("push")!=null )   
 { 
                %>
	<div class="row">  
		<div class="col-lg-12">
          <div class="well bs-component">
        <% if(request.getAttribute("weight")!=null )   
 {
                %>
    	<c:choose>
            <c:when test="${max>=24 && max <=42.0}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Uniquement AirLourd et AirMoyen</p>
            </c:when>
            <c:when test="${max>=42.5 && max <=65}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Vous pouvez que prendre un vol AirLourd a cause du surpoid</p>
            </c:when>
            <c:when test="${max>65.0}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Vous pouvez que prendre un vol AirLourd pour un supplement a la livre</p>
            </c:when>
         </c:choose>
         <% } %>		
		<table id="table_id" class="display compact cell-border" cellspacing="0"> 
		 		<thead>
                    <tr>
       						<th>Ajouter</th>
	                  		<th>Type de vol</th>
	                        <th>Compagnie</th>
	                        <th>Places disponibles</th>
	                        <th>Date de vols</th>
	                        <th>Voir details</th>
                    </tr>
                </thead>
		 	<tbody>			  
		    <c:forEach var="vol" items="${volsAB}">
		      	   <tr>
				      <td><input type="checkbox" id="option_client_active1"  class="form-check-input">
            		  </td>
				      <td> <c:out value="${vol.getName()}" /></td>
				 	  <td> <c:out value="${vol.getFlightCompanyName()}" /></td>
				 	  <c:choose>
		 					<c:when test="${category != null}">
		 						<td> <c:out value="${vol.getNumberOfSeatsByClass(category)-vol.getNumberOfReservedSeatsByClass(category)}" /></td>
            				</c:when>
            				<c:when test="${category == null}">
		 						<td> <c:out value="${vol.getNumberOfSeats()-vol.getNumberOfReservedSeats()}" /></td>
            				</c:when>
           			  </c:choose>
				      <td><c:out value="${vol.getFlightDepartureDate()}" /></td>
				      <td><a href="details?id=${vol.getFlightID()}"> Voir details</a></td> 		
			      </tr>
		    </c:forEach>
		    </tbody>
		</table>
		</div>
		<div class="col-lg-offset-10">
   			<div class="form-group">
                <center><button type="submit" name="searchreturn" id="bouton_texte2" onclick="javascript:afficher_cacher('texte2')" class="btn btn-primary">Rechercher vols de retour</button></center>
            </div>
            <br>
        </div>
		</div>
	</div>
	<div class="row" id="texte2" style="visibility:hidden;">  
		<div class="col-lg-12">
	          <div class="well bs-component">  
	        <% if(request.getAttribute("weight")!=null )   
 {
                %>
	     <c:choose>
            <c:when test="${max>=24 && max <=42.0}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Uniquement AirLourd et AirMoyen</p>
            </c:when>
            <c:when test="${max>=42.5 && max <=65}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Vous pouvez que prendre un vol AirLourd a cause du surpoid</p>
            </c:when>
            <c:when test="${max>65.0}">
           	<p class="alert alert-info alert-dismissible" role="alert" >
			    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    </button>
			    Vous pouvez que prendre un vol AirLourd pour un supplement a la livre</p>
            </c:when>
         </c:choose>
          <% } %>
			 	<table id="table_id2" class="display compact cell-border" cellspacing="0"> 
		 		<thead>
                    <tr>
       						<th>Ajouter</th>
	                  		<th>Type de vol</th>
	                        <th>Compagnie</th>
	                        <th>Places disponibles</th>
	                        <th>Date de vols</th>
	                        <th>Voir details</th>
                    </tr>
                </thead>
		 	<tbody>			  
		    <c:forEach var="vol" items="${volsBA}">
		      	   <tr>
				      <td><input type="checkbox" id="option_client_active1"  class="form-check-input">
            		  </td>
				      <td> <c:out value="${vol.getName()}" /></td>
				 	  <td> <c:out value="${vol.getFlightCompanyName()}" /></td>
				 	  <c:choose>
		 					<c:when test="${category != null}">
		 						<td> <c:out value="${vol.getNumberOfSeatsByClass(category)-vol.getNumberOfReservedSeatsByClass(category)}" /></td>
            				</c:when>
            				<c:when test="${category == null}">
		 						<td> <c:out value="${vol.getNumberOfSeats()-vol.getNumberOfReservedSeats()}" /></td>
            				</c:when>
           			  </c:choose>
				      <td><c:out value="${vol.getFlightDepartureDate()}" /></td>
				      <td><a href="details?id=${vol.getFlightID()}"> Voir details</a></td> 		
			      </tr>
		    </c:forEach>
		    </tbody>
		</table>
			</div>
		</div>
	</div>
	 <% } session.invalidate();%>
<%@include file="footer.jsp" %>