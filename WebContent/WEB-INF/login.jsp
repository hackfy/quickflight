<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="header.jsp" %>
            <form class="form-horizontal" method="POST">
              <div class="row">
                 <div class="col-lg-4">
            	    <div class="well bs-component">
	                  <legend>Customer Log In</legend>
	                  <c:if test="${!empty error}">
	                  <p class="alert alert-info alert-dismissible" role="alert" >
	                  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    		<span aria-hidden="true">x</span>
			    		</button><c:out value="${error}"/></p></c:if>
	                  <div class="form-group">
	                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
	                    <div class="col-lg-12">
	                      <input type="email" name="email" class="form-control" required placeholder="Email">
	                    </div>
	                 </div>
	                  <div class="form-group">
	                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
	                    <div class="col-lg-12">
	                      <input type="password" name="password" class="form-control" required placeholder="Password">
	                    </div>
	                  </div>
		            	<div class="form-group">
		                	<center><button type="submit" id="submit" value="submit" name="submit" class="btn btn-success">Login</button></center>
		                </div>
              </form>
            	<strong><a href="/QuickFlight/register" style="color:white">Register Here</a></strong><br/><br/>
<%@include file="footer.jsp" %>